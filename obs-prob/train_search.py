# Train search model for use in multitasking.

import sys
import search
import numpy as np

rows = 3
cols = 3
output_folder = "output/"

if (len(sys.argv) >= 2):
    rows = int(sys.argv[1])
    rows = int(sys.argv[2])
    output_folder = sys.argv[3]

s = search.search(rows, cols)
#s.log_p = True
s.learn_model(9000000, debug = True)
np.save("models/search-" + str(rows) + "_" + str(cols) + ".npy", s.q)
#s.write_data_to_file(output_folder + str(rows) + "_" + str(cols) + "-training.csv")

