from gym.spaces import Discrete, Dict, Box
import numpy as np

import copy
import random

# Kuskin pään sisäinen representaatio ympäristöstä (auto, tie jne)
class driving_internal():
    def __init__(self, ext_env, params = None):
       self.ext_env = ext_env # The external environment which the driver actually sees

       # How many cells in each direction is the external environment
       # visible to each direction. Note that ext_env.pos_bounds
       # should be set to at least 1 larger that this to avoid "seeing
       # emptiness".
       self.sight_span = 10 # 10 x 10 pikseliä 

       # Waypoints are rectangles for ext_env pos, where the first
       # time arriving in this waypoint triggers a positive reward.
       self.waypoints = []
       self.waypoint_flags = [] # ones for unvisited waypoints

       # Where the task ends
       self.done_area = None

       self.max_time = None

       self.start_speed = 0

       self.has_attention = True # Eli havainnoiko agentti ext_env:iä (silmät auki)

       # Train driving under inattention
       self.training = False # jos True niin agenttia treenattu silmänliiketreenauksen kanssa
       # When training, use this to close / open eyes
       self.attention_switch_prob = 0.05 # Joka stepillä 5% mahis siihen että silmät menee kiinni (ei paras tapa toteuttaa)
                                         # okkluusion pituus riippuu nopeudesta

       self.speed_limit = [27,30] # Changed suitable for the experiments (100-110km/h) (not needed in all experiments)
                                  # action spacesta voi poistaa koko nopeuden mahdollisuus, jos nopeus aina vakio

       self.reset()

    def reset(self, reset_ext_env = True):
        if reset_ext_env:
            self.ext_env.reset()
        self.has_attention = True
        self.inattention_time = 0
        self.ext_env.speed = self.start_speed
        self.waypoint_flags = []
        for i in range(len(self.waypoints)):
            self.waypoint_flags.append(1)
            
        return self.get_belief()

    # Uskomusjakauma käytännössä suoraan mäppäys edestä ja takaa (havainnot)
    def get_belief(self):

        # Inattention time is time since eyes closed, divided by 10 to
        # scale it [0,1] (i.e., assuming there's never over 10s
        # inattention).
        if self.has_attention:
            self.inattention_time = 0
        else:
            if self.inattention_time == 0:
                self.last_seen_pos = copy.deepcopy(self.ext_env.pos)
            self.inattention_time =+ self.ext_env.dt
        
        if self.has_attention:
            x = self.ext_env.pos[0]
            y = self.ext_env.pos[1]
        else:
            x = self.last_seen_pos[0]
            y = self.last_seen_pos[1]            

        xmin = x - self.sight_span
        ymin = y - self.sight_span

        # Add +1 to include the last row and columns
        xmax = x + self.sight_span + 1
        ymax = y + self.sight_span + 1

        obs = self.ext_env.area[int(xmin):int(xmax),int(ymin):int(ymax)] / self.ext_env.max_area_code
        

        # Absolute pos
        # pos = [self.ext_env.pos[0]/self.ext_env.area_bounds,
        #        self.ext_env.pos[1]/self.ext_env.area_bounds]

        # Vague pos
        pos = [round(x/10)/(0.1 * self.ext_env.area_bounds),
               round(y/10)/(0.1 * self.ext_env.area_bounds)]

        self.obs = {'obs': obs,
                    'pos': pos,
                    'inattention': [self.inattention_time / 10],
                    'speed': [self.ext_env.speed / self.ext_env.max_speed],
                    'heading': [self.ext_env.heading / 2*np.math.pi],
                    'steer': [self.ext_env.steer / np.math.pi]}

        return self.obs
    # Kaikki rewardit ovat keskiarvoja asetetun aikarajan sisällä  (esim. jos out of road = -10 rewardia ja 100 sekunin ajon aikana ulkona ollaan oltu 99% ajasta, niin rewardi on -9.90 tästä ajosta)
    def get_reward(self):
        reward = 0
        if self.ext_env.speed < self.speed_limit[0]: # Eli jos observoitu nopeus on pienempi kuin asetettu miniminopeus
            reward -= (self.speed_limit[0] - self.ext_env.speed)/10 # niin MIINUS
        if self.ext_env.speed > self.speed_limit[1]: # Eli jos observoitu nopeus on isompi kuin asetettu maksiminopeus
            reward -= (self.ext_env.speed - self.speed_limit[1])/10 # niin MIINUS
        #i = self.in_waypoint() # i saa erisuuren arvon (kts. in_waypoint funktio) jos waypoint saavutetaan
        #if i != None: # if value of i other than None
        #    self.waypoint_flags[i] = 0 # then change value of that waypoint_flag into 0            
        #    reward += 5 # and give + 1 reward (alkup) HUOM. vaan kun visited eka kerta ### PISTELASKUN TOIMIVUUS???
        if self.ext_env.area[int(self.ext_env.pos[0])][int(self.ext_env.pos[1])] != 1: # Kun ollaan tien ulkopuolella,
            self.ext_env.speed = 5 # eli kun tien ulkopuolella, niin nopeus tippuu automaattisesti 5m/s
            reward -= 1 # niin rewardia -1. Jos 5 < niin ajaa helposti ympyrää
        #if self.within(self.ext_env.pos, self.done_area): # If done area is achieved
        #    reward += 30 #/(1*self.ext_env.time) # then we get reward (in parenthesis based on the time NOT USED) ### DONE AREA SAAVUTETTIIN, MUTTA REWARDI SILTI SUHT SAMA (-0.951) ??? Changed from 20
        #if self.ext_env.limited: # KOMMENTOITU POIS
        #    reward -= 20
        #if self.max_time and self.ext_env.time > self.max_time: # KOMMENTOITU POIS
        #    reward -= 20

        if self.ext_env.pos[0] < 50 or self.ext_env.pos[1] < 50: # TODO: niin että jos ollaan area_boundsin Y ja X -koordinaateilla yli 1600 niin miinusta ja terminal(miten?)
            reward -= 20
            #return self.get_done() #how to terminate state?

        return reward

    def in_waypoint(self):
        for i in range(len(self.waypoints)): # check through all the waypoints
            if self.waypoint_flags[i] == 1 and self.within(self.ext_env.pos, self.waypoints[i]): # if the current waypoint has flag value of 1 ("unvisited")
                return i                                                                         # and we have arrived to the waypoint, return i
        return None # otherwise return None

    def within(self, pos, rect): # Eli tsekkaa tämän avulla onko agentin positio waypointin boundarien sisällä
        return pos[0] >= rect[0] and pos[1] >= rect[1] and pos[0] <= rect[2] and pos[1] <= rect[3] # rect[0 = X-startpoint, 1 = Y-startpoint, 2 = X-endpoint, 3 = Y-endpoint] pos[0 = X, 1 = Y]

    def get_done(self):
        return self.within(self.ext_env.pos, self.done_area) #for looppi withiniin, tarkista joka kymmenennellä tikillä
                                                             #ollaanko forbidden areall
    def add_waypoint(self, rect):
        self.waypoints.append(rect)
        self.waypoint_flags.append(1)

    def step(self, action): # varsinainen steppifunktio (joka wräpätään driving_agentissa)
        if self.training and random.random() < self.attention_switch_prob:
            if self.has_attention == True:
                self.has_attention = False
            else:
                self.has_attention = True

        action[0] = action[0] * self.ext_env.max_acceleration # action spacessa [0] = speed
        action[1] = action[1] * self.ext_env.max_steer # action spacessa [1] = steer
        self.ext_env.step(action)
        return self.get_belief(), self.get_reward(), self.get_done() # ota get done jos haluaa terminal state?

        #POSITIIVISET (rewardit) POIS, ALEMPI NOPEUS JOS POIS TIELTÄ (area code = 2)

