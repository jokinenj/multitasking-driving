# Experiment 2: Lane assist

import numpy as np
import sys

import search
import driver
import multitasking

n = 24
run_time = 1000
softmax_temp = 0.1

rewards = [1,2,10]
lane_assists = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
speeds = [17,33]
search_devices = [[3,2],[3,3]]

output_folder = "exp_out/"

if (len(sys.argv) >= 2):
    rewards = [float(sys.argv[1])]
    lane_assists = [float(sys.argv[2])]
    speeds = [int(sys.argv[3])]
    search_devices = [[int(sys.argv[4]),int(sys.argv[5])]]
    output_folder = sys.argv[6]

d_learn_time = 1000000
m_learn_time = 1000000

for reward in rewards:
    #print(reward)
    for speed in speeds:
        #print(speed)
        for search_device in search_devices:
            #print(search_device)
            for lane_assist in lane_assists:
                #print(lane_assist)

                ## Train the driver
                d = driver.driver(speed)
                d.learn_transitions(iters = 1000)
                d.learn_model(d_learn_time, close_eyes = 0.1, close_duration = 3,
                              anneal_softmax = softmax_temp)

                ## Train the supervisor
                s = search.search(search_device[0], search_device[1])
                s.q = np.load("models/search-" + str(search_device[0]) + "_" +
                                      str(search_device[1]) + ".npy", allow_pickle=True).item()

                ## Learn fist time without lane assist.
                d.clear()
                s.clear()
                d.softmax_temp = softmax_temp
                s.softmax_temp = softmax_temp
                m = multitasking.multitasking(d, s)
                m.reward_scale = reward
                m.learn_model(m_learn_time, anneal_softmax = softmax_temp)

                ## Relearn with lane assist
                d.lane_assist = lane_assist
                m.clear()
                m.softmax_temp = 1
                m.learn_model(m_learn_time, anneal_softmax = softmax_temp)

                m.softmax_temp = softmax_temp
                d.learning = False
                s.learning = False
                m.learning = False
                m.log_p = True

                d.lane_assist = 0
                for model_n in range(n):
                    d.clear()
                    s.clear()
                    m.clear()

                    m.model_n = model_n
                    m.learn_model(run_time)
                    m.write_data_to_file(output_folder + str(reward) + "-" + str(lane_assist) + "-" + str(model_n) + "-" + str(speed) + "-" + str(search_device[0]) + "x" + str(search_device[1]) + ".csv")
                    m.log = []
