import numpy as np
import math
import copy
import map_generator

import matplotlib
import matplotlib.pyplot as plt

class driving_environment():
    def __init__(self, area_bounds = 1000):
        self.wheelbase = 1 # distance of wheels from pos

        self.dt = 0.15 # delta time, a change in time in seconds (model's numerical values are recalculated every 0.15s or 150ms), basically the duration of one timestep
        
        self.trace = [] # iniates trace array

        self.max_steer = 0.08
        self.steer_noise = 0.0
        self.steer_action_noise = 0.0

        # How many m/s can be accelerated in dt time
        self.max_acceleration = 0.5

        self.log = False

        # How many metres from the top right corner does the area span
        self.area_bounds = area_bounds

        self.start_pos = [int(self.area_bounds/2),int(self.area_bounds/2)] # eli agentti lähtee keskeltä karttaa

        #self.start_heading = self.heading #TODO aseta tänne parametriksi lähtöajosuunta


        # How close to the end of the area can the car drive, "invicible wall"
        self.pos_bounds = 20

        # Empty area.
        # 1 = road
        # 2 = out of road
        self.area = np.zeros(shape=(self.area_bounds,self.area_bounds)) + 2 # +2 why?

        self.max_area_code = 2 # Area outside the road (1 = at the road)

        self.max_speed = 30
        self.min_speed = 10

        self.reset()

    def reset(self):
        self.speed = 0

        self.pos = copy.deepcopy(self.start_pos)

        self.trace = []

        self.limited = False # EI TOIMI, pitäisi rajoittaa positio tiettyihin boundeihin, 
                             # ettei agentti lähde liian kauas tiestä (lähinnä debuggaus)        

        self.heading = 4.712 # Tähän radiaaneina se minne suuntaan auto lähtee (0 on itään, pii ≈ 3.141 on länteen jne.) 
        self.steer = 0

        self.time = 0

        self.front_wheel = [self.wheelbase/2,0]
        self.back_wheel = [-self.wheelbase/2,0]

    # Given current position and heading (but not considering current
    # steer), how long until lane crossing?
    def time_to_lane_crossing(self): # Ei välttämättä kutsuta treenauksen vaiheessa koska KALLISTA (kommentoitu tällöin pois animate_trace.py sekä driving_agent.py)
        if self.current_area == 2: # Eli jos ollaan out of bounds
            return 0 # niin TLC = 0 (mutta jostain syystä tämä on aina 0.30?!)
        done = False
        time = 0
        front_wheel = copy.deepcopy(self.front_wheel)
        back_wheel = copy.deepcopy(self.back_wheel)
        pos = copy.deepcopy(self.pos)
        while not done:
            time+=self.dt
            front_wheel[0] = pos[0] + self.wheelbase/2 * math.cos(self.heading)
            front_wheel[1] = pos[1] + self.wheelbase/2 * math.sin(self.heading)
            front_wheel[0] += self.speed * self.dt * math.cos(self.heading + self.steer)
            front_wheel[1] += self.speed * self.dt * math.sin(self.heading + self.steer)
            back_wheel[0] = pos[0] - self.wheelbase/2 * math.cos(self.heading)
            back_wheel[1] = pos[1] - self.wheelbase/2 * math.sin(self.heading)
            back_wheel[0] += self.speed * self.dt * math.cos(self.heading)
            back_wheel[1] += self.speed * self.dt * math.sin(self.heading)            
            
            pos[0] = (front_wheel[0] + back_wheel[0])/2 # eli pos[0] sekä 
            pos[1] = (front_wheel[1] + back_wheel[1])/2 # pos[1] ovat auton etu- ja takarenkaiden välissä
                     
            time += self.dt
            if time > 100: return 100
            if self.current_area(pos = pos) == 2:
                return time


    def current_area(self, pos = None):
        if not pos: pos = self.pos
        if pos[0] < 0 or pos[1] < 0 or pos[0] > self.area_bounds or pos[1] > self.area_bounds: # tarkastaa että positio ei voi olla area_boundsia isompi tai pienempi (= alle 0)
            return 2 # out of road
        return self.area[int(pos[0])][int(pos[1])]

    def update_pos(self):
        steer = np.random.logistic(self.steer, self.steer_noise)
        steer += abs(self.steer)*np.random.logistic(0, self.steer_action_noise)
        steer = max(steer, -self.max_steer)
        steer = min(self.max_steer, steer)
        self.front_wheel[0] = self.pos[0] + self.wheelbase/2 * math.cos(self.heading)
        self.front_wheel[1] = self.pos[1] + self.wheelbase/2 * math.sin(self.heading)

        self.front_wheel[0] += self.speed * self.dt * math.cos(self.heading + steer)
        self.front_wheel[1] += self.speed * self.dt * math.sin(self.heading + steer)

        self.back_wheel[0] = self.pos[0] - self.wheelbase/2 * math.cos(self.heading)
        self.back_wheel[1] = self.pos[1] - self.wheelbase/2 * math.sin(self.heading)

        self.back_wheel[0] += self.speed * self.dt * math.cos(self.heading)
        self.back_wheel[1] += self.speed * self.dt * math.sin(self.heading)

        self.pos[0] = (self.front_wheel[0] + self.back_wheel[0])/2
        self.pos[1] = (self.front_wheel[1] + self.back_wheel[1])/2

        self.limited = False
        if self.pos[0] < self.pos_bounds:
            self.limited = True
            self.pos[0] = self.pos_bounds
        if self.pos[1] < self.pos_bounds:
            self.limited = True
            self.pos[1] = self.pos_bounds
        if self.pos[0] > self.area_bounds - self.pos_bounds:
            self.limited = True
            self.pos[0] = self.area_bounds - self.pos_bounds
        if self.pos[1] > self.area_bounds - self.pos_bounds:
            self.limited = True
            self.pos[1] = self.area_bounds - self.pos_bounds
            
        self.heading = math.atan2(self.front_wheel[1] - self.back_wheel[1],
                                  self.front_wheel[0] - self.back_wheel[0])

    # Action is a vector [speed,steer]
    def step(self, action):
        self.speed += action[0]
        self.speed = max(self.speed, self.min_speed)
        self.speed = min(self.max_speed, self.speed)
        self.steer = action[1]

        self.update_pos()

        state = self.get_state()
        if self.log:
            #state_ = copy.deepcopy(state) # Not needed
            state['tlc'] = self.time_to_lane_crossing()
            self.trace.append([self.time, copy.deepcopy(state)])

        self.time += self.dt # kumuloi jokaisen uuden timestepin (150ms) kokonaisaikaan

        return state

    def get_state(self):
        return {"pos":self.pos,
                "speed":self.speed,
                "heading":self.heading,
                "steer":self.steer}

    def plot_trace(self, full_area = False, waypoints = [], done_area = None):
        if not full_area:
            # Figure out min and max
            x_min = None
            x_max = None
            y_min = None
            y_max = None
            for i in range(len(self.trace)):
                if x_min == None or x_min > self.trace[i][1]["pos"][0]:
                    x_min = int(self.trace[i][1]["pos"][0]) - 1
                if x_max == None or x_max < self.trace[i][1]["pos"][0]:
                    x_max = int(self.trace[i][1]["pos"][0]) + 1
                if y_min == None or y_min > self.trace[i][1]["pos"][1]:
                    y_min = int(self.trace[i][1]["pos"][1]) - 1
                if y_max == None or y_max < self.trace[i][1]["pos"][1]:
                    y_max = int(self.trace[i][1]["pos"][1]) + 1
        else:
            x_min = 0
            x_max = self.area_bounds
            y_min = 0
            y_max = self.area_bounds
            
        
        plot_area = np.zeros(shape = (x_max - x_min, y_max - y_min))

        # Draw the area
        for i in range(len(plot_area)):
            for j in range(len(plot_area[i])):
                plot_area[i][j] = self.area[x_min + i][y_min + j]

        # Draw waypoints?
        for wp in waypoints:
            plot_area[wp[0]:wp[2],wp[1]:wp[3]] = 15
        if done_area:
            plot_area[done_area[0]:done_area[2],done_area[1]:done_area[3]] = 20

        # Draw trace
        for i in range(len(self.trace)):
            plot_area[int(self.trace[i][1]["pos"][0]-x_min)][int(self.trace[i][1]["pos"][1]-y_min)] = 10


        # for i in range(len(self.trace)):
        #     x.append(self.trace[i][1]["pos"][0])
        #     y.append(self.trace[i][1]["pos"][1])
        plt.close()
        #plt.imshow(np.flipud(plot_area.transpose()))
        plt.imshow(plot_area.transpose(), norm = matplotlib.colors.Normalize(vmin = 0, vmax=20))
        # plt.scatter(x, y)
        plt.show()
        return plot_area

# d_env = driving_environment(area_bounds = 1000)
# d_env.speed = 17

# d_env.log = True
# d_env.pos = [100, 500]
# for i in range(100):
#     d_env.step([0,0])
# for i in range(5):
#     d_env.step([0,0.1])
# for i in range(100):
#     d_env.step([0,0])

# for i in range(10):
#     d_env.step([0,0])
# for i in range(10):
#     d_env.step([0,-0.1])
# for i in range(10):
#     d_env.step([1,0])

# d_env.start_pos = [200,210]
# map_generator.make_circular_road(d_env, 500, 100, 700, 50)
# map_generator.make_straight_road(d_env, 100, 500, 700, 500, 50)
# a = d_env.plot_trace(full_area = True)

#TODO: jos x tai y koordinaatti ikinä yli 1600 (jos done_are ylhäällä) niin iso penalty ja terminal. 
#ELI määrittele environmenttiin forbidden areoita ja sitten step funktioon, että jos ollaan siellä niin penalty & terminal.

#tiellä vs out of bounds 100 sekunin sisällä, mikä osuus oikeasta nopeudesta, nopeus voidaan fixata alkuun,
#koita pysyä tiellä ja ajaa ilman waypointeja tai mitään määränpääpistettä.