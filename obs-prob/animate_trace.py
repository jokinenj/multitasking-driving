import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

def animate_trace(env, trace):
    plt.close()
    interval = env.dt

    frames = len(env.trace)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, aspect='equal')
    ax.set_xlim(0, env.area_bounds)
    ax.set_ylim(0, env.area_bounds)


    axl = plt.gca()
    axl.invert_yaxis()

    car = plt.Circle((0, 0), 1, fc='g')
    area = plt.imshow(env.area.transpose(), norm = matplotlib.colors.Normalize(vmin = 0, vmax=20))

    ax.add_artist(area)
    time_text = ax.text(10, 75, "Time:", fontsize=8)
    speed_text = ax.text(10, 175, "Speed:", fontsize=8)
    heading_text = ax.text(10, 275, "Head:", fontsize=8)
    steer_text = ax.text(10, 375, "Steer:", fontsize=8)
    area_text = ax.text(10, 475, "Area:", fontsize=8)
    tlc_text = ax.text(10, 575, "TLC:", fontsize=8)


    def init():
        time_text.set_text('')
        speed_text.set_text('')
        steer_text.set_text('')
        tlc_text.set_text('')
        heading_text.set_text('')
        area_text.set_text('')
        return time_text, speed_text, steer_text, heading_text, area_text, tlc_text

    def animate(i):
        if i == 0:
            ax.add_patch(car)
        t = trace['time'][i]
        t = i * interval
        speed = trace['speed'][i]
        steer = trace['steer'][i]
        tlc = trace['tlc'][i]
        current_area = trace['current_area'][i] # lisätty areakoodi
        heading = trace['heading'][i] #lisätty
        x = int(trace['pos'][i][0]) # pos[0] eli auton x-koordinaatti
        y = int(trace['pos'][i][1]) # pos[1] eli auton y-koordinaatti
        time_text.set_text('Time {:1.2f}'.format(round(t,2)))
        speed_text.set_text('Speed {:1.2f}'.format(round(speed,2)))
        steer_text.set_text('Steer {:1.2f}'.format(round(steer,2)))
        tlc_text.set_text('TLC {:1.2f}'.format(round(tlc,2)))
        area_text.set_text('Area {:1.2f}'.format(round(current_area,2)))
        heading_text.set_text('Heading {:1.2f}'.format(round(heading,2)))
        if trace['has_attention'][i]:
            car.set_color('g')
        else:
            car.set_color('r')
        car.center = (x, y)
        return car, time_text, speed_text, steer_text, heading_text, area_text, tlc_text

    anim = animation.FuncAnimation(
        fig,
        animate,
        init_func=init,
        frames=frames,
        interval=1000 * interval,
        blit=True,
    )

    plt.show()
