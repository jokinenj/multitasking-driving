# Search model

from operator import itemgetter

import random
import numpy as np
import math
from itertools import product

class search():
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.actions = list(product(*[[row for row in range(rows)],
                                      [column for column in range(cols)]]))
        self.cell_distance = 5        # how many visual degrees one cell length is in the device
        self.found_reward = 20        # how much finding the target gives reward

        # The visual search model calculates a lot of distances with a fixed
        # number of parameters. Makes it faster to tabulate.
        self.distances = {}

        # Task type. 0 means that the search target is randomly among
        # the items. 1 means that the target is always the last
        # item that is looked at.
        self.task_type = 0

        self.model_time = 0

        # RL
        self.alpha = 0.1
        self.gamma = 0.9
        self.softmax_temp = 1.0

        self.learning = True

        self.q = {}

        self.log_header = "trial task.type encoding modeltime mt"
        self.log = []
        self.log_p = False

        self.trial = 0

        self.clear()

    def clear(self):
        self.trial += 1
        self.encoding_n = 0
        
        self.randomise_search_device()
        self.obs = obs = np.full([self.rows,self.cols], -1, dtype = int)        
        self.found = False
        self.reward = 0
        self.mt = 0

        # Start with a random eye location but do not observe that location. Reward for the start is 0.
        self.action = random.choice(self.actions)
        self.eye_loc = self.action
        
        self.set_belief_state()
        self.reward = 0

    def randomise_search_device(self):
        self.device = np.zeros([self.rows, self.cols], dtype=int)
        self.task_type = random.choice([0,1])
        if self.task_type == 0:
            row = random.randint(0,self.rows-1)
            col = random.randint(0,self.cols-1)
            self.device[row][col] = 1
            self.target = [row,col]
        else:
            self.target = None
        return self.device

    def observe(self):
        if self.action:
            self.obs[self.action[0],self.action[1]] = self.device[self.action[0],self.action[1]]
        return self.obs

    # Calculate visual distance, as degrees, between two screen
    # coordinates. User distance needs to be given in the same unit.
    def visual_distance(self, start, end, user_distance):
        distance = math.sqrt(pow(start[0] - end[0], 2) + pow(start[1] - end[1],2))
        return 180 * (math.atan(distance / user_distance) / math.pi)
    
    # Eye movement and encoding time come from EMMA (Salvucci, 2001). Also
    # return if a fixation occurred.
    def EMMA_fixation_time(self, distance, freq = 0.1, encoding_noise = False):
        emma_KK = 0.006
        emma_k = 0.4
        emma_prep = 0.135
        emma_exec = 0.07
        emma_saccade = 0.002
        E = emma_KK * -math.log(freq) * math.exp(emma_k * distance)
        if encoding_noise:
            E += np.random.gamma(E, E/3)
        if E < emma_prep: return E, False
        S = emma_prep + emma_exec + emma_saccade * distance
        if (E <= S): return S, True
        E_new = (emma_k * -math.log(freq))
        if encoding_noise:
            E_new += np.random.gamma(E_new, E_new/3)
        T = (1 - (S / E)) * E_new
        return S + T, True

    # Euclidian distance between two points. Use lookuptable for reference
    # or update it with a new entry.
    def distance(self, x1,y1,x2,y2):
        if (x1,y1,x2,y2) not in self.distances:
            self.distances[(x1,y1,x2,y2)] = math.sqrt(pow(x1 - x2, 2) + pow(y1 - y2,2))
        return self.distances[(x1,y1,x2,y2)]

    def learn_model(self, until, debug = False):
        print_time = self.model_time
        long_tasks = 0
        marked_long = False
        while self.model_time < until:

            if not marked_long and self.encoding_n > self.rows*self.cols*+1:
                long_tasks += 1
                marked_long = True
            if self.found:
                marked_long = False
            if debug and self.model_time >= print_time:
                print("Running model...", round(self.model_time / until, 2), len(self.q),
                      round(self.softmax_temp, 2), long_tasks)
                print_time += until / 10
                long_tasks = 0
            self.do_iteration()
            # Anneal softmax temp
            self.softmax_temp = 1-(self.model_time * 0.9 / until)

    def do_iteration(self, debug = False):
        if self.found:
            self.clear()

        self.encoding_n += 1

        self.previous_belief = self.belief
        self.observe()
        self.set_belief_state()

        self.previous_action = self.action
        self.choose_action_softmax()

        self.update_q_sarsa()

        # Move eyes, calculate mt.
        eccentricity = self.distance(self.eye_loc[0], self.eye_loc[1], self.action[0], self.action[1])
        self.mt, moved = self.EMMA_fixation_time(eccentricity*self.cell_distance, encoding_noise = False)        
        if moved:
            self.eye_loc = self.action

        # Calculate reward and mark if target found.
        self.calculate_reward()

        # Learn TD here if found, because next iteration will clear the model.
        if self.found:
            self.update_q_td()

        if self.log_p:
            self.log.append("{} {} {} {} {}".format(self.trial, self.task_type,
                                                    self.encoding_n, self.model_time, self.mt))

    def set_belief_state(self):
        # Must turn belief state into string, otherwise too complicated structure.
        self.belief = repr([self.eye_loc,self.obs])
        # Update Q
        if self.belief not in self.q:
            self.q[self.belief] = {}
            for action in self.actions:
                self.q[self.belief][action] = 0.0

    def calculate_reward(self):
        # Target found.
        if self.device[self.action[0],self.action[1]] == 1:
            self.mt += 0.15 # add motor movement time for response
            self.reward = self.found_reward - self.mt
            self.found = True
        elif self.task_type == 1 and self.obs.mean() == 0:
            # All elements uncovered (no target present).
            self.mt += 0.15 # add motor movement time for response
            self.reward = self.found_reward - self.mt
            self.found = True
        else:
            # Target not yet found.
            self.reward = -self.mt

        self.model_time += self.mt

    def current_q(self):
        return self.q[self.belief][self.action]
        
    def weighted_random(weights):
        number = random.random() * sum(weights.values())
        for k,v in weights.items():
            if number < v:
                break
            number -= v
        return k

    def update_q_sarsa(self):
        if self.learning:
            previous_q = self.q[self.previous_belief][self.previous_action]
            next_q = self.q[self.belief][self.action]
            self.q[self.previous_belief][self.previous_action] = \
                previous_q + self.alpha * (self.reward + self.gamma * next_q - previous_q)

    def update_q_td(self):
        if self.learning:
            previous_q = self.q[self.belief][self.action]
            self.q[self.belief][self.action] = \
                previous_q + self.alpha * (self.reward - previous_q)

    def calculate_max_q_value(self):
        return max(self.q[self.belief].items(), key=itemgetter(1))

    def choose_action_softmax(self):
        if self.softmax_temp == 0:
            self.action = self.calculate_max_q_value()[0]
            return self.action
        p = {}
        try:
            for a in self.q[self.belief].keys():
                p[a] = math.exp(self.q[self.belief][a] / self.softmax_temp)
        except OverflowError:
            self.action = self.calculate_max_q_value()[0]
            return self.action
        
        s = sum(p.values())
        if s != 0:
            p = {k: v/s for k,v in p.items()}
            self.action = search.weighted_random(p)
        else:
            self.action = np.random.choice(list(p.keys()))

    def write_data_to_file(self, filename):
        out = open(filename, "w")
        out.write(self.log_header + "\n")
        for d in self.log:
            out.write(d + "\n")
        out.close()
