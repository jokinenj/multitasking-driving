#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --mem=500
#SBATCH --output=$WRKDIR/task_number_%A_%a.out
#SBATCH --open-mode=append
#SBATCH --array=1-4

n=$SLURM_ARRAY_TASK_ID
iteration=`sed -n "${n} p" slurm_params.txt`
srun python run_experiment.py ${iteration} $WRKDIR/exp_out/


