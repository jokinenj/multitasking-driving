import pygame


BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
LIGHTGRAY = (200, 200, 200)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
LIGHTGREEN = (180, 255, 180)
gap = 2
element_width = 25
element_height = 35
gaze_radius = 5
big_gap = 10

# If visualise only device.
# #Window Information

class Device(object):
    def __init__(self, screen, rows, cols, end_X, start_Y):
        self.rows = rows
        self.cols = cols

        if screen == None:
            pygame.init()
                        
            width = 1200
            gui_height = 250
            height = 600
            size = [width, height + gui_height]
            screen = pygame.display.set_mode(size)
            
        self.font = pygame.font.SysFont("monospace", 15)
        self.screen = screen
        self.done = False
        self.eye_loc = (50, 50)
        self.attention = None
        self.device_width = self.cols * element_width + (self.cols + 1) * gap
        self.deviceHeight = self.rows * element_height + (self.rows + 1) * gap
        self.startX = end_X - self.device_width - big_gap * 2
        self.startY = start_Y
        self.obs_matrix = [-1] * self.rows * self.cols
        # self.screen = pygame.display.set_mode((width, height))

        self.target = self.eye_loc = self.attention = None
        self.elements_pos = []
        self.legend_elements_pos = []
        self.legend = [("Target", GREEN, gaze_radius + 4), ("Attention", BLUE, gaze_radius + 2),
                       ("Eyes", RED, gaze_radius)]

        self.init_elements_pos()
        self.init_legend_pos()

    def init_legend_pos(self):
        for i in range(len(self.legend)):
            self.legend_elements_pos.append(
                (self.startX + 2 * gaze_radius + big_gap, self.startY + self.deviceHeight + big_gap + 25 * i))

    def init_elements_pos(self):
        for r in range(self.rows):
            for c in range(self.cols):
                self.elements_pos.append((self.startX + gap * (c + 1) + c * element_width,
                                          self.startY + gap * (r + 1) + r * element_height))

    def event_detection(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.done = True  # Flag that we are done so we exit this loop
                return

    def draw_elements(self):
        for r in range(self.rows):
            for c in range(self.cols):
                idx = r * self.cols + c

                el_color = LIGHTGREEN if self.obs_matrix[idx] == 0 else LIGHTGRAY
                pygame.draw.rect(self.screen, el_color,
                                 (self.elements_pos[idx][0], self.elements_pos[idx][1], element_width, element_height))

    def sm_to_screen_coords(self, sm_coords):
        if sm_coords == None:
            return None

        return self.startX + int((sm_coords[1] + 1) * gap + (sm_coords[1] + 0.5) * element_width), \
               self.startY + int((sm_coords[0] + 1) * gap + (sm_coords[0] + 0.5) * element_height)

    # def update_sm_data(self, data):
    #     self.eye_loc = self.sm_to_screen_coords(data["eye_loc"])
    #     self.attention = self.sm_to_screen_coords(data["action"])
    #     self.target = self.sm_to_screen_coords(data["target"])

    #     self.obs_matrix = [-1] * self.rows * self.cols if data["obs"] is None else data["obs"]

    def update_sm_data(self, eye_loc, action, target, obs):
        self.eye_loc = self.sm_to_screen_coords(eye_loc)
        self.attention = self.sm_to_screen_coords(action)
        if target:
            self.target = self.sm_to_screen_coords(target)
        else:
            self.target = None

        self.obs_matrix = [-1] * self.rows * self.cols if obs is None else obs
    
    def update_info(self, T, found_n, step, reward):
        self.T = T
        self.found_n = found_n
        self.step = step
        self.reward = reward

    def draw_sm_data(self):
        if self.target != None:
            pygame.draw.circle(self.screen, self.legend[0][1], self.target, self.legend[0][2])
        if self.attention != None and self.is_searching:
            pygame.draw.circle(self.screen, self.legend[1][1], self.attention, self.legend[1][2])
        if self.eye_loc != None and self.is_searching:
            pygame.draw.circle(self.screen, self.legend[2][1], self.eye_loc, self.legend[2][2])

    def draw_legend(self):
        for i in range(len(self.legend)):
            label = self.font.render(self.legend[i][0], 1, BLUE)
            self.screen.blit(label, self.legend_elements_pos[i])
            pygame.draw.circle(self.screen, self.legend[i][1],
                               (self.startX + 5, self.legend_elements_pos[i][1] + self.legend[i][2] * 2), self.legend[i][2])

    def draw_info(self):
        self.screen.blit(self.font.render("CE Time: " + str(round(self.T, 2)), 1, BLUE),
                         (self.startX - 150, self.startY + 10))
        self.screen.blit(self.font.render("Found_N: " + str(self.found_n), 1, BLUE), (self.startX - 150, self.startY + 30))
        self.screen.blit(self.font.render("Step: " + str(self.step), 1, BLUE), (self.startX - 150, self.startY + 50))
        self.screen.blit(self.font.render("Reward: " + str(round(self.reward, 2)), 1, BLUE),
                         (self.startX - 150, self.startY + 70))

    def DrawDevice(self):
        if not self.done:
            self.event_detection()
            pygame.draw.rect(self.screen, BLACK, (self.startX, self.startY, self.device_width, self.deviceHeight))

            self.draw_elements()
            self.draw_sm_data()
            #self.draw_legend()
            #self.draw_info()
            pygame.display.update()

        else:
            pygame.QUIT()
