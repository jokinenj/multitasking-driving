rewards = [5,10,15]
obs_probs = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
speeds = [17,33]
search_devices = [[3,2],[3,3]]

for r in rewards:
    for o in obs_probs:
        for s in speeds:
            for sd in search_devices:
                print(r, o, s, sd[0], sd[1])
