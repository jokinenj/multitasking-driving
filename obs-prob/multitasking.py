# Multitasking model
from operator import itemgetter
import numpy as np
import math
import time
import random

import driver
import search

class multitasking():
    def __init__(self, d, s):
        self.d = d
        self.s = s

        self.task_switch_time = 0.2

        self.actions = ["search","drive"]

        # RL
        self.alpha = 0.1
        self.gamma = 0.9
        self.softmax_temp = 1.0
        self.dyna = 1000

        self.q = {}
        self.tr = {}
        self.learning = True

        self.reward_scale = 1

        self.context = str(d.speed) + "-" + str(s.rows) + "_" + str(s.cols)
        self.log_p = False
        self.log = []
        self.log_header = "context model.n task.n task.type modeltime action reward car.pos"

        self.clear()

    def clear(self):
        self.s.clear()
        self.d.clear()

        self.reward = 0

        self.trial = 1

        self.model_time = 0
        self.action = "drive"

        self.model_n = 0

        self.set_belief_state()

    def learn_model(self, until, debug = False, anneal_softmax = None):
        print_time = self.model_time
        task_times = []
        task_time = 0
        trial_n = self.trial
        reward_sum = 0
        car_pos_sum = 0
        original_softmax_temp = self.softmax_temp
        while self.model_time < until:
            if debug and self.model_time >= print_time:
                if len(task_times) == 0: task_times = [0]
                print("Running model...", round(self.model_time / until, 2), self.trial - trial_n,
                      round(reward_sum), round(car_pos_sum), round(np.mean(task_times),1),
                      round(np.max(task_times),1))
                trial_n = self.trial
                print_time += until / 10
                reward_sum = 0
                car_pos_sum = 0
                task_times = []
            self.do_iteration()
            # Anneal softmax temp
            if anneal_softmax != None:
                self.softmax_temp = original_softmax_temp - \
                    (original_softmax_temp - anneal_softmax)*(self.model_time/until)

            #self.softmax_temp = 1-(self.model_time * 0.9 / until)
            reward_sum += self.reward
            car_pos_sum += abs(self.d.pos)
            if np.mean(self.s.obs) == -1:
                task_times.append(self.model_time - task_time)
                task_time = self.model_time

    def do_iteration(self, debug = False):
        # Record cumulative driver reward.
        sum_reward_d = 0

        self.previous_belief = self.belief
        self.set_belief_state()
        self.previous_action = self.action
        self.choose_action_softmax()

        self.update_q_sarsa()
        #self.update_dyna()

        # In case of task switch, note up the increase in model time
        # and iterate driver.
        if self.previous_action != self.action:
            for i_driving in range(math.floor(self.task_switch_time / self.d.iteration_time)):
                self.d.do_iteration(attention = False)
                sum_reward_d += self.d.reward
            # TODO: Drive the car but don't iterate the model for the remainder.

            self.model_time += self.task_switch_time

        # If action is to drive, let driver do an observation and an
        # action. Each driving iteration takes the same time.
        if self.action == "drive":
            self.d.do_iteration(attention = True)
            sum_reward_d += self.d.reward
            self.model_time += self.d.iteration_time

        # If action is to search, do one search iteration and drive unattended.
        if self.action == "search":
            self.s.do_iteration()
            for i_driving in range(math.floor(self.s.mt / self.d.iteration_time)):
                self.d.do_iteration(attention = False)
                sum_reward_d += self.d.reward
            self.model_time += self.s.mt

        # Reward is the total reward of subtasks
        self.reward = sum_reward_d * self.reward_scale + self.s.reward

        # Reinitialise search model, if target was found. Although the
        # search model would take care of this in its next iteration,
        # if the next multitasking action is to drive, the search
        # model will stay with its found reward, messing with the
        # joint reward calculation.
        if self.s.found == True:
            self.s.clear()
            self.trial += 1

        if self.log_p:
            self.log.append("{} {} {} {} {} {} {} {}".format(self.context, self.model_n, self.trial,
                                                              self.s.task_type, self.model_time, self.action,
                                                              self.reward, self.d.pos))



    def set_belief_state(self):
        self.belief = [self.action, round(self.d.current_q(), 1), round(self.s.current_q(), 1)]
        # Add new belief to Q table is not yet there.
        if self.belief[0] not in self.q:
            self.q[self.belief[0]] = {}
        if self.belief[1] not in self.q[self.belief[0]]:
            self.q[self.belief[0]][self.belief[1]] = {}
        if self.belief[2] not in self.q[self.belief[0]][self.belief[1]]:
            self.q[self.belief[0]][self.belief[1]][self.belief[2]] = {}
            for a in self.actions:
                self.q[self.belief[0]][self.belief[1]][self.belief[2]][a] = 0.0

    def update_q_sarsa(self):
        if self.learning:
            previous_q = self.q[self.previous_belief[0]][self.previous_belief[1]][self.previous_belief[2]][self.previous_action]
            next_q = self.q[self.belief[0]][self.belief[1]][self.belief[2]][self.action]
            self.q[self.previous_belief[0]][self.previous_belief[1]][self.previous_belief[2]][self.previous_action] = previous_q + self.alpha * (self.reward + self.gamma * next_q - previous_q)

    def weighted_random(weights):
        number = random.random() * sum(weights.values())
        for k,v in weights.items():
            if number < v:
                break
            number -= v
        return k

    def calculate_max_q_value(self):
        return max(self.q[self.belief[0]][self.belief[1]][self.belief[2]].items(), key=itemgetter(1))

    def choose_action_softmax(self):
        if self.softmax_temp == 0:
            self.action = self.calculate_max_q_value()[0]
            return self.action

        p = {}
        try:
            for a in self.q[self.belief[0]][self.belief[1]][self.belief[2]].keys():
                p[a] = math.exp(self.q[self.belief[0]][self.belief[1]][self.belief[2]][a] / self.softmax_temp)
        except OverflowError:
            self.action = self.calculate_max_q_value()[0]
            return self.action

        s = sum(p.values())
        if s != 0:
            p = {k: v/s for k,v in p.items()}
            self.action = multitasking.weighted_random(p)
        else:
            self.action = np.random.choice(list(p.keys()))

    def write_data_to_file(self, filename):
        out = open(filename, "w")
        out.write(self.log_header + "\n")
        for d in self.log:
            out.write(d + "\n")
        out.close()

    def update_dyna(self):
        if self.learning == False: return
        pb0 = self.previous_belief[0]
        pb1 = self.previous_belief[1]
        pb2 = self.previous_belief[2]

        b0 = self.belief[0]
        b1 = self.belief[1]
        b2 = self.belief[2]


        if pb0 not in self.tr:
            self.tr[pb0] = {}
        if pb1 not in self.tr[pb0]:
            self.tr[pb0][pb1] = {}
        if pb2 not in self.tr[pb0][pb1]:
            self.tr[pb0][pb1][pb2] = {}

        if self.previous_action not in self.tr[pb0][pb1][pb2]:
            self.tr[pb0][pb1][pb2][self.previous_action] = {}
        if b0 not in self.tr[pb0][pb1][pb2][self.previous_action]:
            self.tr[pb0][pb1][pb2][self.previous_action][b0] = {}
        if b1 not in self.tr[pb0][pb1][pb2][self.previous_action][b0]:
            self.tr[pb0][pb1][pb2][self.previous_action][b0][b1] = {}
        if b2 not in self.tr[pb0][pb1][pb2][self.previous_action][b0][b1]:
            self.tr[pb0][pb1][pb2][self.previous_action][b0][b1][b2] = [self.reward]
        else:
            self.tr[pb0][pb1][pb2][self.previous_action][b0][b1][b2].append(self.reward)

        for i in range(0, self.dyna):
            pb0 = random.choice(list(self.tr.keys()))
            pb1 = random.choice(list(self.tr[pb0].keys()))
            pb2 = random.choice(list(self.tr[pb0][pb1].keys()))
            a = random.choice(list(self.tr[pb0][pb1][pb2].keys()))
            b0 = random.choice(list(self.tr[pb0][pb1][pb2][a].keys()))
            b1 = random.choice(list(self.tr[pb0][pb1][pb2][a][b0].keys()))
            b2 = random.choice(list(self.tr[pb0][pb1][pb2][a][b0][b1].keys()))
            r  = np.mean(self.tr[pb0][pb1][pb2][a][b0][b1][b2])
            #next_q = self.q[pb0][pb1][pb2][a]
            max_q = max(self.q[b0][b1][b2].items(), key=itemgetter(1))[1]
            # Update q based on simulated experience.
            self.q[pb0][pb1][pb2][a] = self.q[pb0][pb1][pb2][a] + \
                self.alpha * (r + self.gamma * max_q - self.q[pb0][pb1][pb2][a])
