from gym import Env
from gym.spaces import Discrete, Dict, Box
import numpy as np

import driving_environment
import driving_internal
import map_generator
import animate_trace

import copy

import importlib
importlib.reload(driving_environment)
importlib.reload(driving_internal)
importlib.reload(map_generator)
importlib.reload(animate_trace)

from stable_baselines3 import PPO # PALJON nopeampi ajaa, mutta ei toimi yhtä hyvin
from stable_baselines3 import SAC # Toimii jatkuviin hienokontrolliongelmiin paremmin kuin PPO? 
                                  #  Sac ei mahdollista diskreettejä toiminta-avaruuksia 
                                  # (lineaarinen toiminta voidaan kuitenkin transformoida diskreetiksi toiminnaksi)
class driving_agent(Env): # Luodaan luokka driving_agent joka periytyy gym-kirjaston Env-luokasta
    def __init__(self, int_env, params = None): # Luodaa luokalle init-funktio, joka assigns values to object properties (or other operations that are necessary to do when the object is being created)
        self.int_env = int_env # Alustaa int_env (=internal environment) propertyn, joka tuodaan argumenttina oliolle
        self.ext_env = int_env.ext_env # Alustaa ext_env (=external environment) propertyn
        self.observation_space = Dict(
            spaces = { # shape = (2,) eli kaksi elementtiä yksiulotteisessa vektorissa, mutta (2,2) olisi 2 x 2 matriisi ja (2,2,2) 2 x 2 x 2 tensori
                'obs': Box(0,1, (2*self.int_env.sight_span+1,
                                 2*self.int_env.sight_span+1), dtype = float), # Observe current lane position
                'inattention': Box(0, 1, (1,), dtype = float), # Attention ON(1)/OFF(0)? => equals ONE observable (1,)
                'pos': Box(0, 1, (2,), dtype = float), # Has two observables (2,)(X & Y) with lows of 0 and highs of 1 (todellinen havaittu positio eli pikseli kentällä skaalattu 0 ja 1 välillä)
                'speed': Box(0, 1, (1,), dtype = float), # Exactly same as before
                'heading': Box(-1, 1, (1,), dtype = float), # Same as before, but possible lowest being -1
                'steer': Box(-1, 1, (1,), dtype = float) # e.g. steer has shape of 1 or one dimensional vector (1,) with low value of -1 and high of 1
            })

        self.action_space = Box(low=-1, high=1, shape = (2,)) # shape of 2 or two dimensional vector (2,) in which a valid observation will be an array of 2 numbers
                                                              # if we change the shape into shape = (1,) we only get steer 

        self.agent = SAC("MultiInputPolicy", self, verbose = 0) # Vaihda joko SAC tai PPO (SAC toimii tässä paremmin)

        self.reset_trace()                      

        self.reset()

    def reset(self):
        self.int_env.reset()
        return self.int_env.reset()

    def reset_trace(self):
         self.trace = {"time": [],
                       "pos": [],
                       "speed": [],
                       "heading": [],
                       "steer": [],
                       "tlc": [],
                       "has_attention": [],
                       "current_area": []}

    def log_trace(self):
        self.trace['time'].append(self.ext_env.time)
        self.trace['pos'].append(copy.deepcopy(self.ext_env.pos))
        self.trace['speed'].append(self.ext_env.speed)
        self.trace['heading'].append(self.ext_env.heading)
        self.trace['steer'].append(self.ext_env.steer)
        self.trace['tlc'].append(self.ext_env.time_to_lane_crossing())
        self.trace['has_attention'].append(self.int_env.has_attention)
        self.trace['current_area'].append(self.ext_env.current_area()) # näyttää ollaanko kaistalla (1) vai pois (2) 

    def step(self, action): # wrapper function (decorator), TSEKKAA TOIMINTA        
        s, r, d = self.int_env.step(action) # miksi s?
        return s, r, d, {} # s = obs?, r = reward, d = done

    # def predict_value(self): # multitaskingia ajatellen >>> policy ennustaa kaikille toiminnoille arvon, joista valitaan atm paras (EI KÄYTÖSSÄ)
    #     values = self.agent.policy.forward(self.agent.policy.obs_to_tensor(self.driver.get_belief())[0])
    #     return values.item()

    def train_agent(self, max_iters = 20, debug = False):
        done = False
        i = 0
        while not done:
            self.agent.learn(total_timesteps = 2000) # Eli 2000 x 150ms (dt) x 20 iteraatiota 
            i+=1
            if debug:
                print(i, self.simulate()) #palauttaa siis iNRO, REWARD, DONE, TIME, SPEED (viimeiset 4 simulate funktion return arvoja)
            if i == max_iters:
                done = True

    def simulate(self, until = 80, get_trace = False, deterministic = True, inattention = []): # simuloidaan/testataan opitun pohjalta mallia 100s
        reward = []
        self.reset()
        done = False
        while not done:
            # Check if this is requested inattention time.
            attention = True
            for i in inattention: # jos argumentiksi esimerkiksi inattention = [[5,8]], niin agentti sulkee simuloinnissa silmät 5 ja 8 sec kohdalla
                if self.ext_env.time > i[0] and self.ext_env.time < i[1]:
                    attention = False
            self.int_env.has_attention = attention
            if get_trace: self.log_trace()
            a, _ = self.agent.predict(self.int_env.obs, deterministic = deterministic)
            _, r, d, _ = self.step(a) # _ notaatiossa viittaa epäkiinnostaviin arvoihin, joista ei haluta mitään palautusta
            reward.append(r)
            if d or self.ext_env.time > until:
                done = True
        if get_trace:
            self.ext_env.log = False
        return round(np.mean(reward),3), d, round(self.ext_env.time), round(self.ext_env.speed) # palauttaa REWARD, DONE, TIME, SPEED

d_env = driving_environment.driving_environment(area_bounds = 1650) # Luodaan driving_environment luokan olio d_env
#d_env = driving_environment.driving_environment(area_bounds = 500)
d_env.start_pos = [100,1600]
d_env.steer_noise = 0.005 # vakionoise, ei välttämättä alkuun tarvi (arvo hatusta heitetty)
d_env.steer_action_noise = 0.005 # ohjauspyörän vääntämiseen liittyvä noise, ei välttämättä alkuun tarvi

# map_generator.make_rectangle_road(d_env, 100, 245, 380, 255)
# map_generator.make_rectangle_road(d_env, 340, 245, 380, 300)
# map_generator.make_rectangle_road(d_env, 380, 245, 390, 500)
map_generator.make_circular_road(d_env, 1600, 1600, 1500, 5) # X = ympyrän keskikohta x-akselilla, Y = ympyrän keskikohta y-akselilla, R = säde, W = width
#map_generator.make_circular_road(d_env, 100, 700, 500, 5)
d_int = driving_internal.driving_internal(d_env) # Luodaan driving_internal luokan olio d_int, jolle parametrinä external environment d_env
                                                 # so the new class will inherit properties and methods from the driving_environment class

# Let's create few different waypoints!
#d_int.add_waypoint([90, 1375, 140, 1425]) # [X-startpoint, Y-startpoint, X-endpoint, Y-endpoint]
#d_int.add_waypoint([120, 1225, 170, 1275])
#d_int.add_waypoint([275, 825, 325, 875])
#d_int.add_waypoint([525, 500, 575, 550])
#d_int.add_waypoint([775, 300, 825, 350])
#d_int.add_waypoint([900, 200, 1000, 300])
#d_int.add_waypoint([1550, 75, 1650, 175])
#d_int.add_waypoint([2200, 200, 2300, 300])
#d_int.add_waypoint([2600, 500, 2700, 600])
#d_int.add_waypoint([2850, 800, 2950, 900])
#d_int.add_waypoint([3000, 1200, 3100, 1300])

#d_int.done_area = [3050, 1550, 3150, 1650] # [X-start, Y-start, X-end, Y-end] TÄMÄ OLISI HYVÄ SAAVUTTAA USEAMPI KERTA!
#d_int.done_area = [50, 1550, 125, 1625] # Done area = starting position, just for fun :--) MAX REWARD: 130.635
d_int.done_area = [1000, 210, 1030, 240] # maali pienemmällä osiolla
d_agent = driving_agent(d_int) # Luodaan driving_agent luokan olio d_agent, joka periytyy driving_internal luokasta
d_int.reset()
d_env.log = True
d_env.trace = []

# for i in range(35):
#     d_int.step([1,0])
# for i in range(53):
#     d_int.step([0,0])
# for i in range(12):
#     d_int.step([0,0.95])
# for i in range(120):
#     d_int.step([0,0])

d_int.start_speed = 27 # Helpottaisko oppimista jos jo speediä alussa
d_agent.train_agent(debug = True, max_iters = 1)
d_agent.simulate(get_trace = True)
d_env.plot_trace(full_area = True, waypoints = d_int.waypoints, done_area = d_int.done_area)
animate_trace.animate_trace(d_env, d_agent.trace)

#driving_agent ajaa ja siis luo agentin joka simuloi ajamista
#importtaa driving_agent ja muokkaa uudessa tiedostossa projektin tarpeisiin
#lisäksi exporttaa trace ja käytä tätä 