# Experiment 1: Observation noise

import numpy as np
import sys

import search
import driver
import multitasking

n = 24
run_time = 1000
softmax_temp = 0.

rewards = [1,2,10]
obs_probs = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
speeds = [17,33]
search_devices = [[3,2],[3,3]]

output_folder = "exp_out/"

if (len(sys.argv) >= 2):
    rewards = [float(sys.argv[1])]
    obs_probs = [float(sys.argv[2])]
    speeds = [int(sys.argv[3])]
    search_devices = [[int(sys.argv[4]),int(sys.argv[5])]]
    output_folder = sys.argv[6]

d_learn_time = 6000
m_learn_time = 4000

for reward in rewards:
    #print(reward)
    for speed in speeds:
        #print(speed)
        for search_device in search_devices:
            #print(search_device)
            for obs_prob in obs_probs:
                #print(obs_prob)

                for model_n in range(n):
                    ## Train the driver
                    d = driver.driver(speed)
                    d.obs_prob = obs_prob
                    d.learn_transitions(iters = 100)
                    d.learn_model(d_learn_time, close_eyes = 0.1, close_duration = 3,
                                  anneal_softmax = softmax_temp)

                    ## Train the supervisor
                    s = search.search(search_device[0], search_device[1])
                    s.q = np.load("models/search-" + str(search_device[0]) + "_" +
                                          str(search_device[1]) + ".npy", allow_pickle=True).item()

                    d.clear()
                    s.clear()

                    d.softmax_temp = softmax_temp
                    s.softmax_temp = softmax_temp

                    m = multitasking.multitasking(d, s)
                    m.reward_scale = reward

                    m.learn_model(m_learn_time, anneal_softmax = softmax_temp)

                    m.softmax_temp = softmax_temp
                    d.learning = False
                    s.learning = False
                    m.learning = False
                    m.log_p = True
                    
                    d.clear()
                    s.clear()
                    m.clear()

                    m.model_n = model_n
                    m.learn_model(run_time)
                    m.write_data_to_file(output_folder + str(reward) + "-" + str(obs_prob) + "-" + str(model_n) + "-" + str(speed) + "-" + str(search_device[0]) + "x" + str(search_device[1]) + ".csv")
                    m.log = []
