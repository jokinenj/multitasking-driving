## Train id-specific models, given sigma posterior.

import driver
import numpy as np
import sys

## Load id-fitted posteriors
id_noise_all = np.load("id_post.npy", allow_pickle=True).item()

## Decide the ids to model: all or a subset based on a param.
ids = id_noise_all.keys()
if (len(sys.argv) >= 2):
    ids = []
    for i in range(1, len(sys.argv)):
        ids.append(sys.argv[i])

## Sample posterior
id_noise = {}
for i in ids:
    id_noise[i] = np.random.choice(id_noise_all[i], 100)

## Train models
master_count = 0
for i in ids:
    count = 0
    for noise in id_noise[i]:
        print("Training", i, round(noise,3), round(count/len(id_noise[i]),3,), round(master_count/len(ids),3))
        count = count + 1
        for speed in [17,33]:
            d = driver.driver(speed, noise)
            d.learn_transitions(iters = 200)
            d.q = np.load("models/driver-pre-" + str(speed) + ".npy", allow_pickle=True).item()
            d.softmax_temp = 0.1
            d.learn_model(2000, anneal_softmax_temp_target = 0.001)
            np.save("id-models/driver-t-" + str(speed) + "-" + str(i) + "-" + str(noise) +
                    ".npy", d.transitions)
            np.save("id-models/driver-" + str(speed) + "-" + str(i) + "-" + str(noise) +
                    ".npy", d.q)
    master_count = master_count + 1

## Run models
master_count = 0
for i in ids:
    count = 0
    for noise in id_noise[i]:
        print("Running", i, round(noise,3), round(count/len(id_noise[i]),3), round(master_count/len(ids),3))
        count = count + 1
        for speed in [17,33]:
            for model in range(12):
                d = driver.driver(speed, noise)
                d.transitions = np.load("id-models/driver-t-" + str(speed) + "-" + str(i) + "-" +
                                        str(noise) + ".npy", allow_pickle=True).item()
                d.q = np.load("id-models/driver-" + str(speed) + "-" + str(i) + "-" +
                              str(noise) + ".npy", allow_pickle=True).item()
                d.log_p = True
                d.softmax_temp = 0.001
                d.learning = False
                d.learn_model(60)
                d.write_data_to_file("id-post-out/driver-" + str(speed) + "-" + str(i) + "-" +
                                     str(noise) + "-" + str(model) + ".csv")
    master_count = master_count + 1    
        
