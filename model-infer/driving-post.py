import driver
import numpy as np
import pandas as pd

import scipy.stats

import logging
logging.basicConfig(level=logging.INFO)

seed = 1
np.random.seed(seed)

import elfi
import GPy

m = elfi.ElfiModel()

sigma = elfi.Prior(scipy.stats.gamma, 3, 0.5, model = m)

Y = elfi.Simulator(driver_generator, sigma, observed = data_o_offset, model = m)

S1 = elfi.Summary(offset_std_60, Y, model = m)
S2 = elfi.Summary(offset_std_120, Y, model = m)

d = elfi.Distance('euclidean', S1, S2, model = m)

log_d = elfi.Operation(np.log, d, model = m)

saved_data = np.load("driving-bolfi.npz")
kernel = GPy.kern.RBF(input_dim=len(m.parameter_names))+GPy.kern.Bias(input_dim=len(m.parameter_names))
m_load = GPy.models.GPRegression(saved_data['X'], saved_data['Y'], kernel=kernel, initialize=False)
m_load.update_model(False)
m_load.initialize_parameter() 
m_load[:] = saved_data['params']
m_load.update_model(True)

target_model = elfi.GPyRegression(m.parameter_names,bounds={'sigma':(0,3)},gp=m_load)

bolfi = elfi.BOLFI(log_d, target_model=target_model)
bolfi.state['n_evidence']=1
bolfi.state['n_batches']=1
bolfi.target_model._kernel_is_default=False

result_BOLFI = bolfi.sample(1000, info_freq=1000)

plt.close()

# m_load.plot()

# result_BOLFI.plot_marginals();

