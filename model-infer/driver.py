# Car driver model

import random
import math
import numpy as np
from operator import itemgetter

class driver():
    def __init__(self, speed):
        self.speed = speed
        self.threshold = 0.875
        self.max_pos = 2

        self.iteration_time = 0.150

        self.P_pos = {}

        self.n_actions = 10
        self.actions = np.linspace(-0.08,0.08, self.n_actions)
        if 0 not in self.actions:
            self.actions = np.append(self.actions, 0)
        self.action = 0.0
        self.action_noise = 0.5

        self.obs_prob = 0.9

        self.transitions = []

        # RL
        self.alpha = 0.1
        self.gamma = 0.9
        self.softmax_temp = 1.0
        self.dyna = 1000

        self.model_time = 0
        
        self.q = {}
        self.tr = {}
        self.learning = True
        
        self.reward = 0

        self.log = []
        self.log_p = False
        self.log_header = "trialtime pos action reward maxq attention"

        self.reward_shaping = 0

        self.clear()

    def clear(self):
        self.pos = 0
        self.model_time = 0
        n_pos = len(np.round(np.arange(-self.max_pos,self.max_pos+0.1, 0.1),1))
        for pos in np.round(np.arange(-self.max_pos,self.max_pos+0.1, 0.1),1):
            self.P_pos[pos] = 1 / n_pos
        self.set_belief_state()

    def learn_transitions(self, iters, debug = False):
        self.transitions = {}
        for pos in self.P_pos:
            self.transitions[pos] = {}
            for a in self.actions:
                self.transitions[pos][a] = {}
                for pos2 in self.P_pos:
                    self.transitions[pos][a][pos2] = 0

        for a in self.actions:
            if debug: print("Creating transitions for", a)
            for pos in self.P_pos:
                # Finer discrete positions within the represented belief.
                for pos_fine in np.linspace(-0.049, 0.049, 10):
                    for i in range(iters):
                        self.pos = pos + pos_fine
                        self.action = a
                        self.update_car_pos(self.iteration_time)
                        new_pos = round(self.pos, 1)                        
                        self.transitions[pos][self.action][new_pos] += 1

        # Normalise transition table.
        for pos in self.P_pos:
            for a in self.actions:
                if sum(self.transitions[pos][a].values()) > 0:
                    factor=1.0/sum(self.transitions[pos][a].values())
                    for pos2 in self.transitions[pos][a].keys():
                        self.transitions[pos][a][pos2] = self.transitions[pos][a][pos2]*factor

        self.clear()

    def learn_model(self, until, close_eyes = 0, close_duration = 5, debug = False,
                    anneal_softmax_temp_target = False):
        if anneal_softmax_temp_target != False:
            original_softmax_temp = self.softmax_temp
        eyes_closed = 0
        sum_reward = 0
        sum_pos = 0
        print_time = self.model_time
        while self.model_time <= until:
            if debug and self.model_time >= print_time:
                print("Running model... ", round(self.model_time / until, 2),
                      len(self.q), len(self.q[self.belief[0]]), round(sum_reward), round(sum_pos))
                print_time += until / 10
                sum_reward = 0
                sum_pos = 0

            # Close eyes?
            if eyes_closed <= 0 and close_eyes > 0 and random.random() < close_eyes:
                eyes_closed = np.random.normal(close_duration, close_duration / 2)
            if eyes_closed > 0:
                eyes_closed -= self.iteration_time
                eyes = False
            else:
                eyes = True
            self.do_iteration(attention = eyes, debug = False)
            # Anneal softmax temp
            if anneal_softmax_temp_target != False:
                self.softmax_temp = original_softmax_temp - (original_softmax_temp - anneal_softmax_temp_target)*(self.model_time/until)                

            sum_reward += self.reward
            sum_pos += abs(self.pos)

    def update_car_pos(self, iteration_time):
        self.steer = self.action + abs(self.action)*np.random.normal(0, self.action_noise)        
        # Steer cannot exceed the maximum of 0.2 radians in any case.
        self.steer = max(-0.2,self.steer)
        self.steer = min(self.steer, 0.2)        
        self.pos += self.speed * iteration_time * math.sin(self.steer)

        # Add slight curve to road, depending on the speed. 17 is from
        # this being calibrated to 60km/h.
        #self.pos += abs(np.random.normal(0, 0.05))*(self.speed/17)

        if self.pos > self.max_pos: self.pos = self.max_pos
        if self.pos < 0 and -self.pos > self.max_pos: self.pos = -self.max_pos

    def do_iteration(self, attention = True, debug = False):
        self.model_time += self.iteration_time

        self.previous_belief = self.belief
        self.update_belief_model()
        if attention:
            self.update_belief_observe()
        self.set_belief_state()
        if debug: print("Belief:", self.belief)

        # Action selection.
        self.previous_action = self.action
        self.choose_action_softmax()

        if debug: print("Action:", self.action)

        # Sarsa learning.
        self.update_q_exp_sarsa()
        #self.update_dyna()

        self.update_car_pos(iteration_time = self.iteration_time)
        self.calculate_reward()

        if debug: print("Pos: ", self.pos, "r", self.reward)

        if self.log_p:
            self.log.append("{} {} {} {} {} {}".format(self.model_time, self.pos, self.action, self.reward,
                                                       max(self.q[self.belief[0]][self.belief[1]].items(),
                                                                                  key=itemgetter(1))[0],
                                                           attention))

    def set_belief_state(self):
        self.belief = [round(max(self.P_pos.items(), key = itemgetter(1))[0], 1),
                       round(driver.entropy_of_dict(self.P_pos), 1)]
        # Add new belief to Q table is not yet there.
        if self.belief[0] not in self.q:
            self.q[self.belief[0]] = {}
        if self.belief[1] not in self.q[self.belief[0]]:
            self.q[self.belief[0]][self.belief[1]] = {}
            for a in self.actions:
                self.q[self.belief[0]][self.belief[1]][a] = 0.0
            
    def update_belief_model(self):
        tmp = {}

        for to_pos in self.P_pos:
            total_prob = 0
            for from_pos in self.transitions:
                    total_prob += self.transitions[from_pos][self.action][to_pos] * self.P_pos[from_pos]
            tmp[to_pos] = total_prob

        s = sum(tmp.values(), 0.0)
        if s > 0: self.P_pos = {k: v / s for k, v in tmp.items()}
        else: self.P_pos = tmp

    def update_belief_observe(self):
        if random.random() < self.obs_prob:
            # True observation
            observed_pos = self.pos
        else:
            observed_pos = random.uniform(-self.max_pos, self.max_pos)

        observed_pos = round(observed_pos, 1)
        for pos in self.P_pos:
            if pos == observed_pos:
                self.P_pos[pos] = self.obs_prob * self.P_pos[pos]
            else:
                self.P_pos[pos] = ((1-self.obs_prob) / (len(self.P_pos) - 1)) * self.P_pos[pos]

        # Normalise
        s = sum(self.P_pos.values(), 0.0)
        if s > 0: self.P_pos = {k: v / s for k, v in self.P_pos.items()}

    def calculate_reward(self):
        #self.reward = -abs(self.pos)
        if abs(self.pos) >= self.threshold:
            self.reward = -1
        else:
            self.reward = 0

        # Reward shaping
        # if self.reward_shaping > 0:
        #     self.reward = self.reward - self.reward_shaping*math.exp(-1/abs(self.pos))        

    def current_q(self):
        return self.q[self.belief[0]][self.belief[1]][self.action]

    def entropy_of_dict(dict):
        ent = 0
        for v in dict.values():
            if v > 0:
                ent += v * math.log(v)
        return -ent

    def weighted_random(weights):
        number = random.random() * sum(weights.values())
        for k,v in weights.items():
            if number < v:
                break
            number -= v
        return k

    def update_q_sarsa(self):
        if self.learning:
            previous_q = self.q[self.previous_belief[0]][self.previous_belief[1]][self.previous_action]
            next_q = self.q[self.belief[0]][self.belief[1]][self.action]
            self.q[self.previous_belief[0]][self.previous_belief[1]][self.previous_action] = \
                previous_q + self.alpha * (self.reward + self.gamma * next_q - previous_q)

    def update_q_exp_sarsa(self):
        if self.learning:
            previous_q = self.q[self.previous_belief[0]][self.previous_belief[1]][self.previous_action]
            next_q = 0
            p = {}
            for a in self.q[self.belief[0]][self.belief[1]].keys():
                p[a] = math.exp(self.q[self.belief[0]][self.belief[1]][a] / self.softmax_temp)            
            for a in self.actions:
                next_q += self.q[self.belief[0]][self.belief[1]][a] * p[a]
                
            self.q[self.previous_belief[0]][self.previous_belief[1]][self.previous_action] = \
                previous_q + self.alpha * (self.reward + self.gamma * next_q - previous_q)
            
    def choose_action_softmax(self):
        p = {}
        for a in self.q[self.belief[0]][self.belief[1]].keys():
            p[a] = math.exp(self.q[self.belief[0]][self.belief[1]][a] / self.softmax_temp)
        s = sum(p.values())
        if s != 0:
            p = {k: v/s for k,v in p.items()}
            self.action = driver.weighted_random(p)
        else:
            self.action = np.random.choice(list(p.keys()))

    def write_data_to_file(self, filename):
        out = open(filename, "w")
        out.write(self.log_header + "\n")
        for d in self.log:
            out.write(d + "\n")
        out.close()

    def update_dyna(self):
        if self.learning == False: return
        pb0 = self.previous_belief[0]
        pb1 = self.previous_belief[1]

        b0 = self.belief[0]
        b1 = self.belief[1]
        
        
        if pb0 not in self.tr:
            self.tr[pb0] = {}
        if pb1 not in self.tr[pb0]:
            self.tr[pb0][pb1] = {}
            
        if self.previous_action not in self.tr[pb0][pb1]:
            self.tr[pb0][pb1][self.previous_action] = {}
        if b0 not in self.tr[pb0][pb1][self.previous_action]:
            self.tr[pb0][pb1][self.previous_action][b0] = {}
        if b1 not in self.tr[pb0][pb1][self.previous_action][b0]:
            self.tr[pb0][pb1][self.previous_action][b0][b1] = [self.reward]
        else:
            self.tr[pb0][pb1][self.previous_action][b0][b1].append(self.reward)

        for i in range(0, self.dyna):
            pb0 = random.choice(list(self.tr.keys()))
            pb1 = random.choice(list(self.tr[pb0].keys()))
            a = random.choice(list(self.tr[pb0][pb1].keys()))
            b0 = random.choice(list(self.tr[pb0][pb1][a].keys()))
            b1 = random.choice(list(self.tr[pb0][pb1][a][b0].keys()))
            r  = np.mean(self.tr[pb0][pb1][a][b0][b1])
            next_q = self.q[pb0][pb1][a]
            max_q = max(self.q[b0][b1].items(), key=itemgetter(1))[1]
            # Update q based on simulated experience.
            self.q[pb0][pb1][a] = self.q[pb0][pb1][a] + \
                self.alpha * (r + self.gamma * max_q - self.q[pb0][pb1][a])
        
