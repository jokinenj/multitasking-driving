## Train models

import driver
import numpy as np

## Pretrain a model
for speed in [17,33]:
    d = driver.driver(speed, 0.1)
    d.learn_transitions(iters = 200, debug = True)
    d.softmax_temp = 1
    d.learn_model(50000, anneal_softmax_temp_target = 0.1, debug = True)
    np.save("models/driver-pre-" + str(speed) + ".npy", d.q)
