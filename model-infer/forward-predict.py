## Make forward predictions of the main task from the practice-based
## inferred sigma posteriors.

import numpy as np
import sys

import search
import driver
import multitasking

n = 12
softmax_temp = 0.1

## Load posterior of noise, and decide the ids to model: all or a
## subset based on a command line param.
id_noise_all = np.load("id_post.npy", allow_pickle=True).item()
ids = id_noise_all.keys()
if (len(sys.argv) >= 2):
    ids = []
    for i in range(1, len(sys.argv)):
        ids.append(sys.argv[i])

## Take median.
id_noise = {}
for i in ids:
    id_noise[i] = np.mean(id_noise_all[i])

for i in id_noise:
    print("Running " + i)
    for speed in [17,33]:
        for search_device in [[3,2],[3,3]]:
            ## Init visual search
            s = search.search(search_device[0], search_device[1])
            s.q = np.load("models/search-" + str(search_device[0]) + "_" +
                          str(search_device[1]) + ".npy", allow_pickle=True).item()

            ## Train driver with the sigma given
            d = driver.driver(speed)
            d.action_noise = id_noise[i]

            #d.reward_shaping = 0.1

            d.learn_transitions(iters = 1000)
            d.learn_model(100000, close_eyes = 0,
                          anneal_softmax_temp_target = 0.1)
            d.clear()
            d.softmax_temp = 1.0
            d.learn_model(100000, close_eyes = 0.05, close_duration = 3,
                          anneal_softmax_temp_target = 0.1)

            ## Train multitasking
            m = multitasking.multitasking(d, s)
            #m.reward_scale = 0.1
            d.softmax_temp = 0.1
            s.softmax_temp = softmax_temp            
            d.learning = False
            s.learning = False
            m.learn_model(200000)

            ## Run the full model
            d.learning = False
            s.learning = False
            m.learning = False
            m.softmax_temp = softmax_temp
            m.log_p = True

            for model_n in range(n):
                s.clear()
                d.clear()
                m.clear()
                m.model_n = model_n
                m.learn_model(1000)
            m.write_data_to_file("forward-out/" + i + "-" + str(speed) + "-" +
                                 str(search_device[0]) + "x" + str(search_device[1]) + ".csv")
