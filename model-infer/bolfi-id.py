## Infer the action_noise parameter for the model individually

import driver
import numpy as np
import pandas as pd

import sys

import scipy.stats
import matplotlib
import matplotlib.pyplot as plt

import logging
logging.basicConfig(level=logging.INFO)

seed = 1
np.random.seed(seed)

## Human data
data_o = pd.read_csv("../data/practice-out.csv", sep=' ').groupby(['id','speed']).std().reset_index()[['id','speed','offset']]

## Driver specs
learn_transitions = 100
start_temp = 0.1
learn_time = 1000
target_temp = 0.001

def driver_generator(sigma, batch_size = 1, random_state = None):
    data = []
    for speed in [17, 33]:
        d = driver.driver(speed, sigma)
        d.learn_transitions(iters = learn_transitions)
        d.q = np.load("models/driver-pre-" + str(speed) + ".npy", allow_pickle=True).item()
        d.softmax_temp = start_temp
        d.learn_model(learn_time, anneal_softmax_temp_target = target_temp)
        d.learning = False
        d.log_p = True
        d.log_as_array = True
        d.softmax_temp = target_temp
        data_speed = []
        for i in range(24):
            d.clear()    
            d.learn_model(100)
            data_speed.append(np.std(d.log))
            d.log = []
        data.append(data_speed)
    return data

def avg_60(data):
    return np.mean(data[0])

def avg_120(data):
    return np.mean(data[1])

import elfi

elfi.new_model()

sigma = elfi.Prior(scipy.stats.gamma, 3, 0, 0.5)

## To plot prior:
## plt.hist(sigma.generate(10000))


ids = np.unique(data_o[['id']])
if (len(sys.argv) >= 2):
    ids = []
    for i in range(1, len(sys.argv)):
        ids.append(sys.argv[i])

for i in ids:
    print(i)
    tmp = data_o[data_o['id'] == i]
    Y = elfi.Simulator(driver_generator, sigma,
                       observed = [tmp[tmp['speed']==60]['offset'],
                                   tmp[tmp['speed']==120]['offset']])

    # This expects that data are already SD, and takes the mean SD.
    # For humans, this is only one value, and for model it's a list of
    # sds from multiple runs.
    S1 = elfi.Summary(avg_60, Y)
    S2 = elfi.Summary(avg_120, Y)

    d = elfi.Distance('euclidean', S1, S2)

    log_d = elfi.Operation(np.log, d)

    bolfi = elfi.BOLFI(log_d, batch_size=1, initial_evidence=20,update_interval=10,
                       bounds={'sigma':(0,3)}, seed = seed)

    bolfi.fit(n_evidence=100)

    np.savez("driving-bolfi-" + i, X=bolfi.target_model.X, Y=bolfi.target_model.Y,
             params=bolfi.target_model._gp.param_array)

