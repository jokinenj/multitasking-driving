
## Train models
for speed in [17,33]:
    for action_noise in np.round(np.arange(0.1,2.1,0.4),2):
        print(speed,action_noise)
        d = driver.driver(speed, action_noise)
        d.learn_transitions(iters = 200, debug = True)
        d.q = np.load("models/driver-pre-" + str(speed) + ".npy", allow_pickle=True).item()
        d.softmax_temp = 0.1
        d.learn_model(2000, anneal_softmax_temp_target = 0.001, debug = True)
        np.save("models/driver-t-" + str(speed) + "-" + str(action_noise) + ".npy", d.transitions)
        np.save("models/driver-" + str(speed) + "-" + str(action_noise) + ".npy", d.q)

## Run models
for speed in [17,33]:
    for action_noise in np.round(np.arange(0.1,2.1,0.4),2):
        print(speed,action_noise)
        for model in range(24):
            d = driver.driver(speed, action_noise)
            d.transitions = np.load("models/driver-t-" + str(speed) +
                                    "-" + str(action_noise) + ".npy", allow_pickle=True).item()
            d.q = np.load("models/driver-" + str(speed) + "-" +
                          str(action_noise) + ".npy", allow_pickle=True).item()
            d.log_p = True
            d.softmax_temp = 0.001
            #d.learning = False
            d.learn_model(100)
            d.write_data_to_file("test-out/driver-" + str(speed) + "-" + str(action_noise) + "-" +
                                 str(model) + ".csv")
        
        
