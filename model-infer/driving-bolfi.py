## Infer the action_noise parameter for the model

import driver
import numpy as np
import pandas as pd

import scipy.stats
import matplotlib
import matplotlib.pyplot as plt

import logging
logging.basicConfig(level=logging.INFO)

seed = 1
np.random.seed(seed)

import elfi
elfi.new_model()

sigma = elfi.Prior(scipy.stats.gamma, 3, 0, 0.5)

## To plot prior:
## plt.hist(sigma.generate(10000))

## Driver specs
learn_transitions = 100
start_temp = 0.1
learn_time = 1000
target_temp = 0.001

def driver_generator(sigma, batch_size = 1, random_state = None):
    data = []
    for speed in [17, 33]:
        d = driver.driver(speed, sigma)
        d.learn_transitions(iters = learn_transitions)
        d.q = np.load("models/driver-pre-" + str(speed) + ".npy", allow_pickle=True).item()
        d.softmax_temp = start_temp
        d.learn_model(learn_time, anneal_softmax_temp_target = target_temp)
        d.learning = False
        d.log_p = True
        d.log_as_array = True
        d.softmax_temp = target_temp
        data_speed = []
        for i in range(24):
            d.clear()
            d.learn_model(100)
            data_speed.append(np.std(d.log))
            d.log = []
        data.append(data_speed)
    return data


## Note that for human data, for simplicity, we aggregate sd across
## each participant without first aggregating within participant. This
## is bad.
##
## Without within-subject pooling:
## 60     0.217324
## 120    0.300090
##
## Done properly:
## 60     0.167976
## 120    0.247012

data_o = pd.read_csv("../data/practice-out.csv", sep=' ')

# data_o_offset = [data_o[['offset','speed']][data_o[['offset','speed']]['speed'] == 60][['offset']],
#                  data_o[['offset','speed']][data_o[['offset','speed']]['speed'] == 120][['offset']]]

tmp = data_o.groupby(['id','speed']).std().reset_index()
data_o_offset = [tmp[tmp['speed'] == 60][['offset']],
                 tmp[tmp['speed'] == 120][['offset']]]

Y = elfi.Simulator(driver_generator, sigma, observed = data_o_offset)

def avg_60(data):
    return np.mean(data[0])

def avg_120(data):
    return np.mean(data[1])

def offset_std_60(data):
    return np.std(data[0])

def offset_std_120(data):
    return np.std(data[1])

# This uses sd from the whole data:
# S1 = elfi.Summary(offset_std_60, Y)
# S2 = elfi.Summary(offset_std_120, Y)

# This expects that data are already SD, and takes the mean SD.
S1 = elfi.Summary(avg_60, Y)
S2 = elfi.Summary(avg_120, Y)

d = elfi.Distance('euclidean', S1, S2)

log_d = elfi.Operation(np.log, d)

bolfi = elfi.BOLFI(log_d, batch_size=1, initial_evidence=20,update_interval=10,
                       bounds={'sigma':(0,3)}, seed = seed)

bolfi.fit(n_evidence=100)

np.savez("driving-bolfi", X=bolfi.target_model.X, Y=bolfi.target_model.Y, params=bolfi.target_model._gp.param_array)



# plt.close()
# #result_BOLFI.plot_traces();
# result_BOLFI.plot_marginals();
# plt.show()

# For setting the kernel:

# span=[bounds[name][1]-bounds[name][0] for name in m.parameter_names]
# kernel = GPy.kern.RBF(input_dim=len(bounds),ARD=True)
# kernel.lengthscale=np.array(span)/5
# for ind in range(len(bounds)): kernel.lengthscale[[ind]].set_prior(GPy.priors.Gamma(2,10/span[ind]), warning=False)
# kernel=kernel+GPy.kern.Bias(input_dim=len(bounds))
# target_model=elfi.GPyRegression(m.parameter_names,bounds=bounds,kernel=kernel)
# bolfi = elfi.BOLFI(d, batch_size=1, initial_evidence=20, update_interval=5, bounds=bounds, target_model=target_model)
