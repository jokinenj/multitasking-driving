from gym.spaces import Discrete, Dict, Box
import numpy as np

import copy
import random

class driving_internal():
    def __init__(self, ext_env, params = None):
       self.ext_env = ext_env # The external environment which the driver actually sees

       # Waypoints are rectangles for ext_env pos, where the first
       # time arriving in this waypoint triggers a positive reward.
       self.waypoints = []
       self.waypoint_flags = [] # ones for unvisited waypoints

       # Terminal area. This provides reward, resulting in the agent
       # wanting to reach this position as fast as possible, while
       # obeying speed limits and avoiding off-road driving.
       self.done_area = None

       self.max_time = None

       self.start_speed = 0

       self.has_attention = True
       self.can_occlude = True

       # How much inattention gets reward each tick.
       self.inattention_reward = 0.1

       # Train driving under inattention
       self.training = False
       # When training, use this to close / open eyes

       self.speed_limit = [27,30]

       self.reset()

    def reset(self, reset_ext_env = True):
        if reset_ext_env:
            self.ext_env.reset()
        self.has_attention = True
        self.inattention_time = 0
        self.ext_env.speed = self.start_speed
        self.waypoint_flags = []
        for i in range(len(self.waypoints)):
            self.waypoint_flags.append(1)

        # Internal representation of the car, used for internally
        # simulating driving during inattention.
        self.obs_belief = {}
        self.obs_belief['pos'] = copy.copy(self.ext_env.pos)
        self.obs_belief['front_wheel'] = self.ext_env.front_wheel
        self.obs_belief['back_wheel'] = self.ext_env.back_wheel
        self.obs_belief['heading'] = self.ext_env.heading

        return self.get_belief()

    def get_belief(self):

        # Inattention time is time since eyes closed, divided by 10 to
        # scale it [0,1] (i.e., assuming there's never over 10s
        # inattention).
        if self.has_attention:
            self.inattention_time = 0
        else:
            if self.inattention_time == 0:
                self.last_seen_pos = copy.deepcopy(self.ext_env.pos)
            self.inattention_time =+ self.ext_env.dt

        if self.has_attention:
            # Fully observable


            # x = self.ext_env.pos[0]
            # y = self.ext_env.pos[1]
            # xmin = x - self.sight_span
            # ymin = y - self.sight_span

            # # Add +1 to include the last row and columns
            # xmax = x + self.sight_span + 1
            # ymax = y + self.sight_span + 1

            # obs = self.ext_env.area[int(xmin):int(xmax),int(ymin):int(ymax)] / self.ext_env.max_area_code

            self.obs_belief['pos'] = copy.copy(self.ext_env.pos)
            self.obs_belief['front_wheel'] = self.ext_env.front_wheel
            self.obs_belief['back_wheel'] = self.ext_env.back_wheel
            self.obs_belief['heading'] = self.ext_env.heading

            # For purposes of getting tlc, we need the current area.
            # If it's 2 (out of bounds), we want to calculate the
            # distance back to road, and present that info as
            # negative. If we're on the road, distance should be to
            # out of bounds.
            area = self.ext_env.current_area()
            # if area == 1:
            #     _area = 2
            #     m = 1
            # else:
            #     _area = 1
            #     m = -1

            m = 1
            _area = 2
            tlc =  self.ext_env.time_to_lane_crossing()
            tlc_l = self.ext_env.time_to_lane_crossing(heading = 0.8)
            tlc_r = self.ext_env.time_to_lane_crossing(heading = -0.8)
            tlc_l2 = self.ext_env.time_to_lane_crossing(heading = 1.6)
            tlc_r2 = self.ext_env.time_to_lane_crossing(heading = -1.6)
        else:
            # Belief update based on most likely values and no noise.
            pos, f_w, b_w, heading = \
                self.ext_env.simulate_pos_update(self.ext_env.speed,
                                                 self.obs_belief['pos'],
                                                 self.ext_env.steer,
                                                 0, 0,
                                                 self.ext_env.max_steer,
                                                 self.obs_belief['heading'],
                                                 self.obs_belief['front_wheel'],
                                                 self.obs_belief['back_wheel'],
                                                 self.ext_env.wheelbase,
                                                 self.ext_env.dt)

            area = self.ext_env.current_area(pos = pos)
            # if area == 1:
            #     _area = 2
            #     m = 1
            # else:
            #     _area = 1
            #     m = -1
            m = 1
            _area = 2                
            tlc = self.ext_env.time_to_lane_crossing(pos = self.obs_belief['pos'])
            tlc_l = self.ext_env.time_to_lane_crossing(pos = self.obs_belief['pos'], heading = 0.8)
            tlc_r = self.ext_env.time_to_lane_crossing(pos = self.obs_belief['pos'], heading = -0.8)
            tlc_l2 = self.ext_env.time_to_lane_crossing(pos = self.obs_belief['pos'], heading = 1.6)
            tlc_r2 = self.ext_env.time_to_lane_crossing(pos = self.obs_belief['pos'], heading = -1.6)


            # x = self.last_seen_pos[0]
            # y = self.last_seen_pos[1]
            # xmin = x - self.sight_span
            # ymin = y - self.sight_span

            # # Add +1 to include the last row and columns
            # xmax = x + self.sight_span + 1
            # ymax = y + self.sight_span + 1

            # obs = self.ext_env.area[int(xmin):int(xmax),int(ymin):int(ymax)] / self.ext_env.max_area_code
            # obs = self.ext_env.area[int(xmin):int(xmax),int(ymin):int(ymax)] / self.ext_env.max_area_code

            self.obs_belief['pos'] = pos
            self.obs_belief['front_wheel'] = f_w
            self.obs_belief['back_wheel'] = b_w
            self.obs_belief['heading'] = heading

        # Normalise obs
        self.obs = {#'obs': obs,
            'area': [area/self.ext_env.max_area_code],
            'tlc': [tlc/10,tlc_l/10,tlc_l2/10,tlc_r/10,tlc_r2/10],
            #'tlc': [tlc/self.ext_env.max_tlc,tlc_l/self.ext_env.max_tlc,tlc_r/self.ext_env.max_tlc],
            # 'pos': [self.obs_belief['pos'][0] / self.ext_env.area_bounds,
            #         self.obs_belief['pos'][1] / self.ext_env.area_bounds],
            'inattention': [self.inattention_time / 10],
            'speed': [self.ext_env.speed / self.ext_env.max_speed],
            'heading': [self.obs_belief['heading'] / (2*np.math.pi)],
            'steer': [self.ext_env.steer / np.math.pi]}

        return self.obs

    def get_reward(self):
        reward = 0
        if self.has_attention == False:
            reward += self.inattention_reward
        if self.ext_env.speed < self.speed_limit[0]:
            reward -= (self.speed_limit[0] - self.ext_env.speed) / 10
        if self.ext_env.speed > self.speed_limit[1]:
            reward -= (self.ext_env.speed - self.speed_limit[1]) / 10

        if self.ext_env.area[int(self.ext_env.pos[0])][int(self.ext_env.pos[1])] != 1:
            #self.ext_env.speed = 5
            reward -= 1

        if self.within(self.ext_env.pos, self.done_area):
            reward += 10

        return reward

    def get_current_area(self):
        return self.ext_env.area[int(self.ext_env.pos[0])][int(self.ext_env.pos[1])]

    def in_waypoint(self):
        for i in range(len(self.waypoints)):
            if self.waypoint_flags[i] == 1 and self.within(self.ext_env.pos, self.waypoints[i]):
                return i
        return None

    def within(self, pos, rect):
        return pos[0] >= rect[0] and pos[1] >= rect[1] and pos[0] <= rect[2] and pos[1] <= rect[3]

    def get_done(self):
        return self.within(self.ext_env.pos, self.done_area)

    def add_waypoint(self, rect):
        self.waypoints.append(rect)
        self.waypoint_flags.append(1)

    def step(self, action):
        self.action = action

        action[0] = action[0] * self.ext_env.max_acceleration
        action[1] = action[1] * self.ext_env.max_steer
        if action[2] < 0 and self.can_occlude:
            self.has_attention = False
        else:
            self.has_attention = True
        self.ext_env.step(action)
        return self.get_belief(), self.get_reward(), self.get_done()
