from gym import Env
from gym.spaces import Discrete, Dict, Box
import numpy as np

import driving_environment
import driving_internal
import map_generator
import animate_trace

import copy

import importlib
importlib.reload(driving_environment)
importlib.reload(driving_internal)
importlib.reload(map_generator)
importlib.reload(animate_trace)

from stable_baselines3 import PPO
from stable_baselines3 import SAC

class driving_agent(Env):
    def __init__(self, int_env, params = None):
        self.int_env = int_env
        self.ext_env = int_env.ext_env

        # Change log_id between multiple episodes to individuate them
        # in trace.
        self.log_id = "id0"

        self.observation_space = Dict(
            spaces = {
                # 'obs': Box(0,1, (2*self.int_env.sight_span+1,
                #                  2*self.int_env.sight_span+1), dtype = float),
                'area': Box(0, 1, (1,), dtype=float),
                'tlc': Box(-1, 1, (5,), dtype = float),
                #'pos': Box(0, 1, (2,), dtype = float),
                'inattention': Box(0, 1, (1,), dtype = float),
                'speed': Box(0, 1, (1,), dtype = float),
                'heading': Box(-1, 1, (1,), dtype = float),
                'steer': Box(-1, 1, (1,), dtype = float)
            })

        # [steer, accelerate, attention]
        self.action_space = Box(low=-1, high=1, shape = (3,))

        # How many seconds can one episode run.
        self.max_episode_time = 80

        self.max_oob_time = 0.1

        self.agent = PPO("MultiInputPolicy", self, verbose = 0)

        self.reset_trace()

        self.reset()

    def reset(self):
        self.current_oob_time = 0
        self.int_env.reset()
        return self.int_env.reset()

    def reset_trace(self):
         self.trace = {'id': [],
                       "time": [],
                       "pos": [],
                       "speed": [],
                       "heading": [],
                       "steer": [],
                       "tlc": [],
                       "tlc_l": [],
                       "tlc_r": [],
                       "has_attention": [],
                       "current_area": []}

    def log_trace(self):
        self.trace['id'].append(self.log_id)
        self.trace['time'].append(self.ext_env.time)
        self.trace['pos'].append(copy.deepcopy(self.ext_env.pos))
        self.trace['speed'].append(self.ext_env.speed)
        self.trace['heading'].append(self.ext_env.heading)
        self.trace['steer'].append(self.ext_env.steer)
        self.trace['tlc'].append(self.ext_env.time_to_lane_crossing())
        self.trace['tlc_l'].append(self.ext_env.time_to_lane_crossing(heading = 0.8))
        self.trace['tlc_r'].append(self.ext_env.time_to_lane_crossing(heading = -0.8))
        self.trace['has_attention'].append(self.int_env.has_attention)
        self.trace['current_area'].append(self.ext_env.current_area()) # näyttää ollaanko kaistalla (1) vai pois (2)

    def step(self, action):
        s, r, d = self.int_env.step(action)
        if self.ext_env.time > self.max_episode_time:
            d = True
        if self.int_env.get_current_area() != 1:
            self.current_oob_time += self.ext_env.dt
            if self.max_oob_time and self.current_oob_time > self.max_oob_time:
                d = True
        else:
            self.current_oob_time = 0
        return s, r, d, {}

    # def predict_value(self):
    #     #values = self.agent.policy.forward(self.agent.policy.obs_to_tensor(self.driver.get_belief())[0])
    #     return values.item()

    def train_agent(self, max_iters = 20, debug = False):
        done = False
        i = 0
        while not done:
            self.agent.learn(total_timesteps = 50000)
            i+=1
            if debug:
                print(i, self.simulate())
            if i == max_iters:
                done = True

    def train_curriculum(self, total_timesteps = 100000):
        # Start with immediate resetting if oob, no occlusion.
        self.int_env.can_occlude = False
        self.max_oob_time = 0.1
        self.agent.learn(total_timesteps = total_timesteps)
        # Then ease off max oob time
        self.max_oob_time = 1
        self.agent.learn(total_timesteps = total_timesteps)
        self.max_oob_time = 2
        self.agent.learn(total_timesteps = total_timesteps)
        self.max_oob_time = 4
        self.agent.learn(total_timesteps = total_timesteps)
        # Then same with occlude
        self.int_env.can_occlude = True
        self.max_oob_time = 0.1
        self.agent.learn(total_timesteps = total_timesteps)
        # Then ease off max oob time
        self.agent.learn(total_timesteps = total_timesteps)
        self.max_oob_time = 2
        self.agent.learn(total_timesteps = total_timesteps)
        self.max_oob_time = 4
        self.agent.learn(total_timesteps = total_timesteps)

    def simulate_n(self, n = 10, until = 60, deterministic = False):
        done = 0
        for i in range(n):
            _, d, _, _, = self.simulate(until = until, deterministic = deterministic)
            if d == True:
                done += 1
        return done/n

    def simulate(self, until = 60, get_trace = False, deterministic = True, inattention = [], print_out = False, print_prefix = "0"):
        reward = []
        self.reset()
        done = False
        while not done:
            if get_trace: self.log_trace()
            a, _ = self.agent.predict(self.int_env.obs, deterministic = deterministic)
            _, r, d, _ = self.step(a)
            reward.append(r)
            if d or self.ext_env.time > until:
                done = True
            if print_out:
                print(self.log_id + " " + print_prefix + " " + str(self.ext_env.time) + " " +
                      str(self.ext_env.speed) +
                      " " + str(self.ext_env.heading) + " " + str(self.ext_env.steer) + " " +
                      str(self.ext_env.current_area()) + " " +
                      ("1" if self.int_env.has_attention else "0") + " " +
                      str(self.int_env.inattention_time) + " " +
                      str(self.ext_env.pos[0]) + " " + str(self.ext_env.pos[1]))
                      
        if get_trace:
            self.ext_env.log = False
        return round(np.mean(reward),3), self.int_env.get_done(), round(self.ext_env.time), round(self.ext_env.speed)

# d_env = driving_environment.driving_environment(area_bounds = 1650)
# d_env.adas.append('lca')
# d_env.start_heading = 5.2
# # d_env.start_pos = [845,305] # very close to finish area
# d_env.start_pos = [327,808] # farther away
# d_env.steer_noise = 0.005
# d_env.steer_action_noise = 0.005

# map_generator.make_circular_road(d_env, 1600, 1600, 1500, 5)
# d_int = driving_internal.driving_internal(d_env)

# #d_int.add_waypoint([850, 280, 875, 310])
# #d_int.add_waypoint([925, 250, 950, 275])

# d_int.done_area = [1000, 210, 1030, 240]
# d_agent = driving_agent(d_int)
# d_int.start_speed = 28
# d_int.reset()

# d_agent.train_curriculum()
# d_env.log = True
# d_env.trace = []
# d_agent.max_oob_time = 5
# d_agent.simulate_n(n = 100)
# d_agent.simulate(get_trace = True, deterministic = False)
# d_env.plot_trace(full_area = True, waypoints = d_int.waypoints, done_area = d_int.done_area)
# animate_trace.animate_trace(d_env, d_agent.trace)
