## Functions for generating areas for driving.

#from skimage.draw import line_aa
import math

def make_rectangle_road(env, x0, y0, x1, y1):
    env.area[x0:x1,y0:y1] = 1

def make_circular_road(env, x0, y0, r, w): #x0 & y0 being the middle points of the circle for each axis
    xmin = x0-(r+w)
    ymin = y0-(r+w)
    xmax = x0+(r+w)
    ymax = y0+(r+w)

    x = max(xmin,0)
    while x <= xmax and x < env.area_bounds:
        y = max(ymin,0)
        while y <= ymax and y < env.area_bounds:
            d = dist([x,y], [x0, y0]) 
            if d > r - w/2 and d < r + w/2:
                env.area[x,y] = 1
            y += 1
        x += 1

def dist(loc1, loc2):
    return math.sqrt(math.pow(loc1[0]-loc2[0],2) + math.pow(loc1[1]-loc2[1],2))

# def make_straight_road(env, x0, y0, x1, y1, w):
#     for i in range(w):
#         _i = i-i/2
#         _x0 = min(x0+_i, env.area_bounds-1)
#         _x1 = min(x1+_i, env.area_bounds-1)
#         _y0 = min(y0+_i, env.area_bounds-1)
#         _y1 = min(y1+_i, env.area_bounds-1)        
#         rr, cc, val = line_aa(int(_x0), int(_y0), int(_x1), int(_y1))
#         env.area[rr,cc] = val.astype(int)*1
    
# def make_straight_road_(env, x0, y0, x1, y1):
#     rr, cc, val = line_aa(x0, y0, x1, y1)
#     env.area[rr,cc] = val.astype(int)*1

# A simple area that has a large road in the middle
def simple_area(env):
    max_i = env.area_bounds/2
    width = env.area_bounds/10

    env.area[0:env.area_bounds, int(max_i-width), int(max_i+width)] = 2
    

# # From https://stackoverflow.com/questions/31638651/how-can-i-draw-lines-into-numpy-arrays
# def trapez(y,y0,w):
#     return np.clip(np.minimum(y+1+w/2-y0, -y+1+w/2+y0),0,1)

# def make_straight_road_(env, r0, c0, r1, c1, w, rmin=0, rmax=np.inf):
#     # The algorithm below works fine if c1 >= c0 and c1-c0 >= abs(r1-r0).
#     # If either of these cases are violated, do some switches.
#     if abs(c1-c0) < abs(r1-r0):
#         # Switch x and y, and switch again when returning.
#         xx, yy, val = weighted_line(c0, r0, c1, r1, w, rmin=rmin, rmax=rmax)
#         return (yy, xx, val)

#     # At this point we know that the distance in columns (x) is greater
#     # than that in rows (y). Possibly one more switch if c0 > c1.
#     if c0 > c1:
#         return weighted_line(r1, c1, r0, c0, w, rmin=rmin, rmax=rmax)

#     # The following is now always < 1 in abs
#     slope = (r1-r0) / (c1-c0)

#     # Adjust weight by the slope
#     w *= np.sqrt(1+np.abs(slope)) / 2

#     # We write y as a function of x, because the slope is always <= 1
#     # (in absolute value)
#     x = np.arange(c0, c1+1, dtype=float)
#     y = x * slope + (c1*r0-c0*r1) / (c1-c0)

#     # Now instead of 2 values for y, we have 2*np.ceil(w/2).
#     # All values are 1 except the upmost and bottommost.
#     thickness = np.ceil(w/2)
#     yy = (np.floor(y).reshape(-1,1) + np.arange(-thickness-1,thickness+2).reshape(1,-1))
#     xx = np.repeat(x, yy.shape[1])
#     vals = trapez(yy, y.reshape(-1,1), w).flatten()

#     yy = yy.flatten()

#     # Exclude useless parts and those outside of the interval
#     # to avoid parts outside of the picture
#     mask = np.logical_and.reduce((yy >= rmin, yy < rmax, vals > 0))

#     rr = yy[mask].astype(int)
#     cc = xx[mask].astype(int)
#     vv = vals[mask]
    
#     env.area[rr,cc] = vv.astype(int)*5

#     return

    
    
    
            
        
    
