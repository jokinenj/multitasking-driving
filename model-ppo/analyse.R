##
library(tidyverse)

data <- read.table("out.out", header = F, sep = "") %>%
    rename(speed = V1,
           obs.prob = V2,
           action.noise = V3,
           steering.noise = V4,
           oob.reward = V5,
           cols = V6,
           rows = V7,
           found.reward = V8,
           switch = V9,
           oob = V10,
           off.road = V11,
           task.time = V12)

ggplot(data, aes(factor(steering.noise), switch, fill = factor(oob.reward))) +
    geom_boxplot() +
    facet_grid(cols ~ rows)
