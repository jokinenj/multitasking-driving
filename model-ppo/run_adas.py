# Run ADAS experiments

import argparse

import sys
import importlib

import driving_environment
import driving_internal
import map_generator
import animate_trace
import driving_agent

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--steer_noise", type=float, default=0.005)
    parser.add_argument("--steer_action_noise", type=float, default=0.005)
    parser.add_argument("--lca", type=int, default = 0)
    parser.add_argument("--min_speed", type=float, default = 0)
    parser.add_argument("--max_speed", type=float, default = 50)
    parser.add_argument("--start_speed", type=float, default = 30)
    parser.add_argument("--total_timesteps", type=float, default = 100000)
    parser.add_argument("--n_episodes", type=float, default = 12)

    args = parser.parse_args()

    d_env = driving_environment.driving_environment(area_bounds = 1650)

    if args.lca == 1:
        d_env.adas.append('lca')

    d_env.start_heading = 5.2
    d_env.start_pos = [327,808]
    d_env.steer_noise = args.steer_noise
    d_env.steer_action_noise = args.steer_action_noise
    map_generator.make_circular_road(d_env, 1600, 1600, 1500, 5)

    d_int = driving_internal.driving_internal(d_env)
    d_int.done_area = [1000, 210, 1030, 240]
    d_agent = driving_agent.driving_agent(d_int)
    d_int.start_speed = args.start_speed
    d_int.speed_limit[0] = args.min_speed
    d_int.speed_limit[1] = args.max_speed
    d_int.reset()
    d_agent.train_curriculum(total_timesteps = args.total_timesteps)

    d_agent.log_id = str(args.max_speed) + " " + str(args.lca)
    for n in range(args.n_episodes):
        d_agent.simulate(print_out = True, print_prefix = str(n))

def print_adas_params():
    for max_speed in ["17","30"]:
        for lca in ["True", "False"]:
            print("--max_speed=" + max_speed + " --lca=" + lca)
