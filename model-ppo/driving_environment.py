import numpy as np
import math
import copy
import map_generator

# import matplotlib
# import matplotlib.pyplot as plt

class driving_environment():
    def __init__(self, area_bounds = 1000):
        self.wheelbase = 1 # distance of wheels from pos

        self.dt = 0.15 # delta time, a change in time in seconds (model's numerical values are recalculated every 0.15s or 150ms), basically the duration of one timestep

        self.trace = [] # iniates trace array

        self.max_steer = 0.08
        self.steer_noise = 0.0
        self.steer_action_noise = 0.0

        self.start_heading = 0

        # How many m/s can be accelerated in dt time
        self.max_acceleration = 0.5

        self.max_tlc = 50

        self.log = False

        # How many metres from the top right corner does the area span
        self.area_bounds = area_bounds

        self.start_pos = [0,0]

        # How close to the end of the area can the car drive, "invicible wall"
        self.pos_bounds = 20

        # Empty area.
        # 1 = road
        # 2 = out of road
        self.area = np.zeros(shape=(self.area_bounds,self.area_bounds)) + 2 # +2 why?

        self.max_area_code = 2 # Area outside the road (1 = at the road)

        self.max_speed = 30
        self.min_speed = 10

        self.adas = []

        self.reset()

    def reset(self, heading = False):
        self.speed = 0

        self.pos = copy.copy(self.start_pos)

        self.trace = []

        self.limited = False


        self.heading = self.start_heading
        self.steer = 0

        self.time = 0

        self.front_wheel = [self.wheelbase/2,0]
        self.back_wheel = [-self.wheelbase/2,0]

    # Given current position and heading (but not considering current
    # steer), how long until lane crossing?
    # def time_to_lane_crossing(self):
    #     if self.current_area() == 2:
    #         return 0
    #     done = False
    #     time = 0
    #     front_wheel = copy.deepcopy(self.front_wheel)
    #     back_wheel = copy.deepcopy(self.back_wheel)
    #     pos = copy.deepcopy(self.pos)
    #     while not done:
    #         time+=self.dt
    #         front_wheel[0] = pos[0] + self.wheelbase/2 * math.cos(self.heading)
    #         front_wheel[1] = pos[1] + self.wheelbase/2 * math.sin(self.heading)
    #         front_wheel[0] += self.speed * self.dt * math.cos(self.heading + self.steer)
    #         front_wheel[1] += self.speed * self.dt * math.sin(self.heading + self.steer)
    #         back_wheel[0] = pos[0] - self.wheelbase/2 * math.cos(self.heading)
    #         back_wheel[1] = pos[1] - self.wheelbase/2 * math.sin(self.heading)
    #         back_wheel[0] += self.speed * self.dt * math.cos(self.heading)
    #         back_wheel[1] += self.speed * self.dt * math.sin(self.heading)

    #         pos[0] = (front_wheel[0] + back_wheel[0])/2
    #         pos[1] = (front_wheel[1] + back_wheel[1])/2

    #         time += self.dt
    #         if time > 100: return 100
    #         if self.current_area(pos = pos) == 2:
    #             return time


    def current_area(self, pos = None):
        if not pos: pos = self.pos
        if pos[0] < 0 or pos[1] < 0 or pos[0] > self.area_bounds or pos[1] > self.area_bounds:
            return 2 # out of road
        return self.area[int(pos[0])][int(pos[1])]

    def update_pos(self):
        steer = np.random.logistic(self.steer, self.steer_noise)
        if 'lca' not in self.adas:
            steer += abs(self.steer)*np.random.logistic(0, self.steer_action_noise)
        steer = max(steer, -self.max_steer)
        steer = min(self.max_steer, steer)
        
        self.front_wheel[0] = self.pos[0] + self.wheelbase/2 * math.cos(self.heading)
        self.front_wheel[1] = self.pos[1] + self.wheelbase/2 * math.sin(self.heading)

        self.front_wheel[0] += self.speed * self.dt * math.cos(self.heading + steer)
        self.front_wheel[1] += self.speed * self.dt * math.sin(self.heading + steer)

        self.back_wheel[0] = self.pos[0] - self.wheelbase/2 * math.cos(self.heading)
        self.back_wheel[1] = self.pos[1] - self.wheelbase/2 * math.sin(self.heading)

        self.back_wheel[0] += self.speed * self.dt * math.cos(self.heading)
        self.back_wheel[1] += self.speed * self.dt * math.sin(self.heading)

        self.pos[0] = (self.front_wheel[0] + self.back_wheel[0])/2
        self.pos[1] = (self.front_wheel[1] + self.back_wheel[1])/2

        self.limited = False
        if self.pos[0] < self.pos_bounds:
            self.limited = True
            self.pos[0] = self.pos_bounds
        if self.pos[1] < self.pos_bounds:
            self.limited = True
            self.pos[1] = self.pos_bounds
        if self.pos[0] > self.area_bounds - self.pos_bounds:
            self.limited = True
            self.pos[0] = self.area_bounds - self.pos_bounds
        if self.pos[1] > self.area_bounds - self.pos_bounds:
            self.limited = True
            self.pos[1] = self.area_bounds - self.pos_bounds

        self.heading = math.atan2(self.front_wheel[1] - self.back_wheel[1],
                                  self.front_wheel[0] - self.back_wheel[0])

    # Lane watch works by returning a steer that, within limits,
    # attempts to maximise TLC.
    def lca(self):        
        # Can be set to smaller values for a less aggressive lane watch.
        # steers = np.linspace(-self.max_steer, self.max_steer, 10)
        # steers = np.append(steers, 0)

        steers = [-0.05, 0, 0.05]
        headings = [-0.2,0,0.2]
        # Select the TLC that best, our of four steers (current steer,
        # 0, min or max) maximises TLC.
        tlc = {}
        for i in range(len(headings)):
            tlc[steers[i]] = self.time_to_lane_crossing(heading = headings[i])

        # If we're off-road, lane assist does nothing.
        if max(tlc) <= 0:
            return self.steer
        #print(tlc)
        return max(tlc, key = tlc.get)


    def simulate_pos_update(self, speed, pos, steer, steer_noise, steer_action_noise, max_steer, heading, front_wheel, back_wheel, wheelbase, dt):
        steer = np.random.logistic(steer, steer_noise)
        steer += abs(steer)*np.random.logistic(0, steer_action_noise)
        steer = max(steer, -max_steer)
        steer = min(max_steer, steer)
        front_wheel[0] = pos[0] + wheelbase/2 * math.cos(heading)
        front_wheel[1] = pos[1] + wheelbase/2 * math.sin(heading)

        front_wheel[0] += speed * dt * math.cos(heading + steer)
        front_wheel[1] += speed * dt * math.sin(heading + steer)

        back_wheel[0] = pos[0] - wheelbase/2 * math.cos(heading)
        back_wheel[1] = pos[1] - wheelbase/2 * math.sin(heading)

        back_wheel[0] += speed * dt * math.cos(heading)
        back_wheel[1] += speed * dt * math.sin(heading)

        pos[0] = (front_wheel[0] + back_wheel[0])/2
        pos[1] = (front_wheel[1] + back_wheel[1])/2


        heading = math.atan2(front_wheel[1] - back_wheel[1],
                             front_wheel[0] - back_wheel[0])

        return pos, front_wheel, back_wheel, heading


    # Action is a vector [speed,steer]
    def step(self, action):
        self.speed += action[0]
        self.speed = max(self.speed, self.min_speed)
        self.speed = min(self.max_speed, self.speed)
        self.steer = action[1]

        if 'lca' in self.adas:
            self.steer = self.lca()        

        self.update_pos()

        state = self.get_state()
        if self.log:
            state['tlc'] = self.time_to_lane_crossing()
            self.trace.append([self.time, copy.deepcopy(state)])

        self.time += self.dt

        return state

    def get_state(self):
        return {"pos":self.pos,
                "speed":self.speed,
                "heading":self.heading,
                "steer":self.steer}

    # def plot_trace(self, full_area = False, waypoints = [], done_area = None):
    #     plt.close()
    #     if not full_area and self.trace:
    #         # Figure out min and max
    #         x_min = None
    #         x_max = None
    #         y_min = None
    #         y_max = None
    #         for i in range(len(self.trace)):
    #             if x_min == None or x_min > self.trace[i][1]["pos"][0]:
    #                 x_min = int(self.trace[i][1]["pos"][0]) - 1
    #             if x_max == None or x_max < self.trace[i][1]["pos"][0]:
    #                 x_max = int(self.trace[i][1]["pos"][0]) + 1
    #             if y_min == None or y_min > self.trace[i][1]["pos"][1]:
    #                 y_min = int(self.trace[i][1]["pos"][1]) - 1
    #             if y_max == None or y_max < self.trace[i][1]["pos"][1]:
    #                 y_max = int(self.trace[i][1]["pos"][1]) + 1
    #     else:
    #         x_min = 0
    #         x_max = self.area_bounds
    #         y_min = 0
    #         y_max = self.area_bounds


    #     plot_area = np.zeros(shape = (x_max - x_min, y_max - y_min))

    #     # Draw the area
    #     for i in range(len(plot_area)):
    #         for j in range(len(plot_area[i])):
    #             plot_area[i][j] = self.area[x_min + i][y_min + j]

    #     # Draw waypoints?
    #     for wp in waypoints:
    #         plot_area[wp[0]:wp[2],wp[1]:wp[3]] = 15
    #     if done_area:
    #         plot_area[done_area[0]:done_area[2],done_area[1]:done_area[3]] = 20

    #     # Draw trace
    #     for i in range(len(self.trace)):
    #         plot_area[int(self.trace[i][1]["pos"][0]-x_min)][int(self.trace[i][1]["pos"][1]-y_min)] = 20

    #     # If not trace, draw current pos
    #     if not self.trace:
    #         plot_area[int(self.pos[0])][int(self.pos[1])] = 10

    #     # for i in range(len(self.trace)):
    #     #     x.append(self.trace[i][1]["pos"][0])
    #     #     y.append(self.trace[i][1]["pos"][1])
    #     #plt.imshow(np.flipud(plot_area.transpose()))
    #     plt.imshow(plot_area.transpose(), norm = matplotlib.colors.Normalize(vmin = 0, vmax=20))
    #     # plt.scatter(x, y)
    #     plt.show()
    #     return

    # Use Bresenham algorithm to calculate how many seconds away and
    # end cell is in the area, given starting location and heading.
    # Returns a number of cells until that area, but only up to max_n.
    # By default, this is time until lane crossing.
    def time_to_lane_crossing(self, max_n = 10, heading = 0, end = 2, pos = None):
        if not max_n:
            max_n = self.max_tlc
        if self.speed == 0:
            return max_n
        if not pos:
            pos = self.pos
        x0, y0 = int(pos[0]), int(pos[1])
        if self.current_area([x0,y0]) == end:
            return 0
        heading = self.heading + heading
        x1 = x0 + math.cos(heading) * max_n
        y1 = y0 + math.sin(heading) * max_n
        dx = abs(x1 - x0)
        dy = abs(y1 - y0)
        sx = 1 if x0 < x1 else -1
        sy = 1 if y0 < y1 else -1
        error = dx - dy

        i = 0
        while True:
            i += 1
            if i == max_n:
                break
            e2 = 2 * error
            if e2 > -dy:
                error -= dy
                x0 += sx
            if e2 < dx:
                error += dx
                y0 += sy
            if self.current_area([x0,y0]) == end:
                break

        # Make sure not to get too large numbers with a very small
        # speed.
        return i / max(self.speed,1)


# d_env = driving_environment(area_bounds = 1000)
# d_env.speed = 17

# d_env.log = True
# d_env.pos = [100, 500]
# for i in range(100):
#     d_env.step([0,0])
# for i in range(5):
#     d_env.step([0,0.1])
# for i in range(100):
#     d_env.step([0,0])

# for i in range(10):
#     d_env.step([0,0])
# for i in range(10):
#     d_env.step([0,-0.1])
# for i in range(10):
#     d_env.step([1,0])

# d_env.start_pos = [200,210]
# map_generator.make_circular_road(d_env, 500, 100, 700, 50)
# map_generator.make_straight_road(d_env, 100, 500, 700, 500, 50)
# a = d_env.plot_trace(full_area = True)


# def plot_trace(trace, area, full_area = False):
#     if not full_area and trace:
#         # Figure out min and max
#         x_min = None
#         x_max = None
#         y_min = None
#         y_max = None
#         for i in range(len(trace)):
#             if x_min == None or x_min > trace["pos"][i][0]:
#                 x_min = int(trace["pos"][i][0]) - 1
#             if x_max == None or x_max < trace["pos"][i][0]:
#                 x_max = int(trace["pos"][i][0]) + 1
#             if y_min == None or y_min > trace["pos"][i][1]:
#                 y_min = int(trace["pos"][i][1]) - 1
#             if y_max == None or y_max < trace["pos"][i][1]:
#                 y_max = int(trace["pos"][i][1]) + 1
#     else:
#         x_min = 0
#         x_max = full_area[0]
#         y_min = 0
#         y_max = full_area[1]


#     plot_area = np.zeros(shape = (x_max - x_min, y_max - y_min))

#     # Draw the area
#     for i in range(len(plot_area)):
#         for j in range(len(plot_area[i])):
#             plot_area[i][j] = area[x_min + i][y_min + j]

#     # Draw trace
#     for i in range(len(trace['time'])):
#         if trace['has_attention'][i]:
#             col = 100
#         else:
#             col = 2
#         plot_area[int(trace["pos"][i][0]-x_min)][int(trace["pos"][i][1]-y_min)] = col

#     # for i in range(len(trace)):
#     #     x.append(trace[i][1]["pos"][0])
#     #     y.append(trace[i][1]["pos"][1])
#     plt.close()
#     #plt.imshow(np.flipud(plot_area.transpose()))
#     plt.imshow(plot_area.transpose(), norm = matplotlib.colors.Normalize(vmin = 0, vmax=20))
#     # plt.scatter(x, y)
#     plt.show()

# d_env = driving_environment(area_bounds = 1650)
# d_env.adas.append('lca')
# d_env.start_heading = 5.3
# d_env.start_pos = [327,808] # farther away
# d_env.steer_noise = 0.005
# d_env.steer_action_noise = 0.005

# map_generator.make_circular_road(d_env, 1600, 1600, 1500, 5)

# d_env.reset()
# d_env.speed = 20
# d_env.log = True
# d_env.trace = []
# for i in range(500):
#     d_env.step([0,0.1])
# d_env.plot_trace()
