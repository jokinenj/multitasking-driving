# Train multitasking model.

import numpy as np

import search
import driver
import multitasking

learn_time = 100000

print("Training multitasking model 33 3 4...")
s = search.search(3, 4)
s.q = np.load("models/search-3_4.npy").item()
d = driver.driver(33)
d.transitions = np.load("models/driver-t-33.npy").item()
d.q = np.load("models/driver-33.npy").item()
d.learning = False
s.learning = False
d.softmax_temp = 0.1
s.softmax_temp = 0.1
m = multitasking.multitasking(d, s)
m.log_p = True
m.learn_model(learn_time, debug = True)
np.save("models/multi33-3_4.npy", m.q)
m.write_data_to_file("output/multi-33-3_4-training.csv")

print("Training multitasking model 17 3 4...")
s = search.search(3, 4)
s.q = np.load("models/search-3_4.npy").item()
d = driver.driver(17)
d.transitions = np.load("models/driver-t-17.npy").item()
d.q = np.load("models/driver-17.npy").item()
d.learning = False
s.learning = False
d.softmax_temp = 0.1
s.softmax_temp = 0.1
m = multitasking.multitasking(d, s)
m.log_p = True
m.learn_model(learn_time, debug = True)
np.save("models/multi17-3_4.npy", m.q)
m.write_data_to_file("output/multi-17-3_4-training.csv")

print("Training multitasking model 17 3 2...")
s = search.search(3, 2)
s.q = np.load("models/search-3_2.npy").item()
d = driver.driver(17)
d.transitions = np.load("models/driver-t-17.npy").item()
d.q = np.load("models/driver-17.npy").item()
d.learning = False
s.learning = False
d.softmax_temp = 0.1
s.softmax_temp = 0.1
m = multitasking.multitasking(d, s)
m.log_p = True
m.learn_model(learn_time, debug = True)
np.save("models/multi17-3_2.npy", m.q)
m.write_data_to_file("output/multi-17-3_2-training.csv")

print("Training multitasking model 33 3 2...")
s = search.search(3, 2)
s.q = np.load("models/search-3_2.npy").item()
d = driver.driver(33)
d.transitions = np.load("models/driver-t-33.npy").item()
d.q = np.load("models/driver-33.npy").item()
d.learning = False
s.learning = False
d.softmax_temp = 0.1
s.softmax_temp = 0.1
m = multitasking.multitasking(d, s)
m.log_p = True
m.learn_model(learn_time, debug = True)
np.save("models/multi33-3_2.npy", m.q)
m.write_data_to_file("output/multi-33-3_2-training.csv")
