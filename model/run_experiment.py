# Experiment 1: Validation to human data.

import numpy as np

import search
import driver
import multitasking

n = 12
run_time = 1000
softmax_temp = 0.1

print("Running multitasking model 33 3 4...")
for model_n in range(n):
    print(model_n)
    s = search.search(3, 4)
    s.q = np.load("models/search-3_4.npy").item()
    d = driver.driver(33)
    d.transitions = np.load("models/driver-t-33.npy").item()
    d.q = np.load("models/driver-33.npy").item()
    m = multitasking.multitasking(d, s)
    m.q = np.load("models/multi33-3_4.npy").item()
    d.learning = False
    s.learning = False
    m.learning = False
    d.softmax_temp = softmax_temp
    s.softmax_temp = softmax_temp
    m.softmax_temp = softmax_temp
    m.log_p = True
    m.learn_model(run_time, debug = True)
    m.write_data_to_file("exp1-out/multi-33-3_4-model-" + str(model_n) + ".csv")


print("Running multitasking model 17 3 4...")
for model_n in range(n):
    print(model_n)
    s = search.search(3, 4)
    s.q = np.load("models/search-3_4.npy").item()
    d = driver.driver(17)
    d.transitions = np.load("models/driver-t-17.npy").item()
    d.q = np.load("models/driver-17.npy").item()
    m = multitasking.multitasking(d, s)
    m.q = np.load("models/multi17-3_4.npy").item()
    d.learning = False
    s.learning = False
    m.learning = False
    d.softmax_temp = softmax_temp
    s.softmax_temp = softmax_temp
    m.softmax_temp = softmax_temp
    m.log_p = True
    m.learn_model(run_time, debug = True)
    m.write_data_to_file("exp1-out/multi-17-3_4-model-" + str(model_n) + ".csv")

print("Running multitasking model 17 3 2...")
for model_n in range(n):
    print(model_n)
    s = search.search(3, 2)
    s.q = np.load("models/search-3_2.npy").item()
    d = driver.driver(17)
    d.transitions = np.load("models/driver-t-17.npy").item()
    d.q = np.load("models/driver-17.npy").item()
    m = multitasking.multitasking(d, s)
    m.q = np.load("models/multi17-3_2.npy").item()
    d.learning = False
    s.learning = False
    m.learning = False
    d.softmax_temp = softmax_temp
    s.softmax_temp = softmax_temp
    m.softmax_temp = softmax_temp
    m.log_p = True
    m.learn_model(run_time, debug = True)
    m.write_data_to_file("exp1-out/multi-17-3_2-model-" + str(model_n) + ".csv")

print("Running multitasking model 33 3 2...")
for model_n in range(n):
    print(model_n)
    s = search.search(3, 2)
    s.q = np.load("models/search-3_2.npy").item()
    d = driver.driver(33)
    d.transitions = np.load("models/driver-t-33.npy").item()
    d.q = np.load("models/driver-33.npy").item()
    m = multitasking.multitasking(d, s)
    m.q = np.load("models/multi33-3_2.npy").item()
    d.learning = False
    s.learning = False
    m.learning = False
    d.softmax_temp = softmax_temp
    s.softmax_temp = softmax_temp
    m.softmax_temp = softmax_temp
    m.log_p = True
    m.learn_model(run_time, debug = True)
    m.write_data_to_file("exp1-out/multi-33-3_2-model-" + str(model_n) + ".csv")    
