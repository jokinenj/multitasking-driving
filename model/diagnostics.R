##
library(tidyverse)

## Multitasking model training
data <- read.table("output/multi-33-3_3-training.csv", header = T, sep = "")

ggplot(data %>%
       group_by(task.n, task.type) %>%
       mutate(attention.switch = ifelse((action == "search" & lag(action == "drive")) |
                                        (action == "search" & is.na(lag(action))), 1, 0)) %>%
       summarise(task.time = max(modeltime - min(modeltime)),
                 car.pos = sd(car.pos),
                 attention.switch = mean(attention.switch),
                 reward = mean(reward)) %>%
       ungroup() %>%
       na.omit() %>%
       mutate(task.n = ntile(task.n, 100)) %>%
       group_by(task.n, task.type) %>%
       summarise(task.time = mean(task.time),
                 car.pos = mean(car.pos),
                 attention.switch = mean(attention.switch),
                 reward = mean(reward)),
       aes(task.n, reward, colour = factor(task.type))) +
    geom_point() +
    geom_line() +
    geom_smooth(se = F, method = "lm")

## Search model training
data <- read.table("output/search-3_3-training.csv", header = T, sep = "")

ggplot(data %>%
       group_by(trial, task.type) %>%
       summarise(tasktime = max(modeltime) - min(modeltime),
                 encoding = max(encoding)) %>%
       ungroup() %>%
       mutate(trial = ntile(trial, 10)) %>%
       group_by(trial, task.type) %>%
       summarise(tasktime = mean(tasktime),
                 encoding = mean(encoding)),
       aes(trial, encoding, colour = factor(task.type))) +
    geom_point() +
    geom_line()

## Driving training
data <- read.table("output/driver-33-training.csv", header = T, sep = "")

ggplot(data %>%
       mutate(trialtime = ntile(trialtime, 100)) %>%
       group_by(attention, trialtime) %>%
       summarise(sd.pos = sd(pos),
                 pos = mean(abs(pos)),
                 reward = mean(reward),
                 maxq = mean(maxq),
                 sd.action = sd(action),
                 action = mean(abs(action))),
       aes(trialtime, reward, colour = attention)) +
    geom_point() +
    geom_line()

## Investigate running out of raod
tmp <- data %>%    
    mutate(eyes_closed = ifelse(attention == "False" & lag(attention == "True"), 1, 0)) %>%
                         #ifelse(attention == "False" & lead(attention) == "True", 2, 0))) %>%
    filter(eyes_closed > 0 | pos < 0.875) %>%
    filter(attention == "False")
