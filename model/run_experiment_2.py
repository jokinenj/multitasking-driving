## Experiment 2: The effect of observation probability on multitasking behaviour.

import numpy as np
import os

import search
import driver
import multitasking

learn_time = 300000

## Train the driver?
d = driver.driver(33)
d.obs_prob = 0.1
if not os.path.isfile("models/driver-t-33-exp2-01.npy"):
    d.learn_transitions(iters = 1000, debug = True)
    d.learn_model(learn_time, close_eyes = 0.1, close_duration = 3, debug = True)
    np.save("models/driver-t-33-exp2-01.npy", d.transitions)
    np.save("models/driver-33-exp2-01.npy", d.q)

## If necessary, reinit driver, load search model and train the
## multitasking model.
if not os.path.isfile("models/multi-exp2-01.npy"):
    d = driver.driver(33)
    d.obs_prob = 0.1
    d.transitions = np.load("models/driver-t-33-exp2-01.npy").item()
    d.q = np.load("models/driver-33-exp2-01.npy").item()
    s = search.search(3, 4)
    s.q = np.load("models/search-3_4.npy").item()
    m = multitasking.multitasking(d, s)    
    d.learning = False
    s.learning = False
    d.softmax_temp = 0.1
    s.softmax_temp = 0.1
    m.learn_model(learn_time, debug = True)
    np.save("models/multi-exp2-01.npy", m.q)

## Load all models, run the experiment.
for model_n in range(12):
    d = driver.driver(33)
    d.obs_prob = 0.1
    d.transitions = np.load("models/driver-t-33-exp2-01.npy").item()
    d.q = np.load("models/driver-33-exp2-01.npy").item()
    s = search.search(3, 4)
    s.q = np.load("models/search-3_4.npy").item()
    m = multitasking.multitasking(d, s)
    m.q = np.load("models/multi-exp2-01.npy").item()
    d.learning = False
    s.learning = False
    m.learning = False
    d.softmax_temp = 0.1
    s.softmax_temp = 0.1
    m.softmax_temp = 0.1
    m.log_p = True
    m.learn_model(1000, debug = True)
    m.write_data_to_file("exp2-out/01-" + str(model_n) + ".csv")

## Repeat for obs_prob = 0.5
## Train the driver?
d = driver.driver(33)
d.obs_prob = 0.5
if not os.path.isfile("models/driver-t-33-exp2-05.npy"):
    d.learn_transitions(iters = 1000, debug = True)
    d.learn_model(learn_time, close_eyes = 0.1, close_duration = 3, debug = True)
    np.save("models/driver-t-33-exp2-05.npy", d.transitions)
    np.save("models/driver-33-exp2-05.npy", d.q)

## If necessary, reinit driver, load search model and train the
## multitasking model.
if not os.path.isfile("models/multi-exp2-05.npy"):
    d = driver.driver(33)
    d.obs_prob = 0.5
    d.transitions = np.load("models/driver-t-33-exp2-05.npy").item()
    d.q = np.load("models/driver-33-exp2-05.npy").item()
    s = search.search(3, 4)
    s.q = np.load("models/search-3_4.npy").item()
    m = multitasking.multitasking(d, s)    
    d.learning = False
    s.learning = False
    d.softmax_temp = 0.1
    s.softmax_temp = 0.1
    m.learn_model(learn_time, debug = True)
    np.save("models/multi-exp2-05.npy", m.q)

## Load all models, run the experiment.
for model_n in range(12):
    d = driver.driver(33)
    d.obs_prob = 0.5
    d.transitions = np.load("models/driver-t-33-exp2-05.npy").item()
    d.q = np.load("models/driver-33-exp2-05.npy").item()
    s = search.search(3, 4)
    s.q = np.load("models/search-3_4.npy").item()
    m = multitasking.multitasking(d, s)
    m.q = np.load("models/multi-exp2-05.npy").item()
    d.learning = False
    s.learning = False
    m.learning = False
    d.softmax_temp = 0.1
    s.softmax_temp = 0.1
    m.softmax_temp = 0.1
    m.log_p = True
    m.learn_model(1000, debug = True)
    m.write_data_to_file("exp2-out/05-" + str(model_n) + ".csv")


