import pygame

# Edited code from: https://stackoverflow.com/questions/38551168/radio-button-in-pygame
class Checkbox:
    def __init__(self, screen, font, x, y, size = 15, color=(230, 230, 230), caption="", outline_color=(0, 0, 0),
                 check_color=(0, 0, 0), font_size=22, font_color=(0, 0, 0), text_offset=(28, 1)):
        self.screen = screen
        self.x = x
        self.y = y
        self.color = color
        self.caption = caption
        self.oc = outline_color
        self.cc = check_color
        self.fs = font_size
        self.fc = font_color
        self.to = text_offset
        self.font = font
        self.size = size
        # checkbox object
        self.checkbox_obj = pygame.Rect(self.x, self.y, self.size, self.size)
        self.checkbox_outline = self.checkbox_obj.copy()
        # variables to test the different states of the checkbox
        self.checked = False
        self.active = False
        self.unchecked = True
        self.click = False

    def _draw_button_text(self):
        self.font_surf = self.font.render(self.caption, True, self.fc)
        self.font_pos = (self.x + self.size + 5, self.y)
        self.screen.blit(self.font_surf, self.font_pos)

    def render_checkbox(self):
        pygame.draw.rect(self.screen, self.color, self.checkbox_obj)
        pygame.draw.rect(self.screen, self.oc, self.checkbox_outline, 1)
        if self.checked:
            pygame.draw.circle(self.screen, self.cc, (self.x + int(self.size / 2), self.y + int(self.size / 2)), int(self.size / 3))

        self._draw_button_text()

    def _update(self, event_object):
        x, y = event_object.pos
        # self.x, self.y, 12, 12
        px, py, w, h = self.checkbox_obj  # getting check box dimensions
        if px < x < px + w and px < x < px + w:
            self.active = True
        else:
            self.active = False

    def _mouse_up(self):
            if self.active and not self.checked and self.click:
                    self.checked = True
            elif self.checked:
                self.checked = False
                self.unchecked = True

            if self.click is True and self.active is False:
                if self.checked:
                    self.checked = True
                if self.unchecked:
                    self.unchecked = True
                self.active = False

    def update_checkbox(self, event_object):
        if event_object.type == pygame.MOUSEBUTTONDOWN:
            self.click = True
            # self._mouse_down()
        if event_object.type == pygame.MOUSEBUTTONUP:
            self._mouse_up()
        if event_object.type == pygame.MOUSEMOTION:
            self._update(event_object)