import pygame
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (0, 255, 50)
GREY = (200, 200, 200)
TRANS = (1, 1, 1)

# Edited code from: http://www.dreamincode.net/forums/topic/401541-buttons-and-sliders-in-pygame/
class Slider():
    def __init__(self, screen, font, name, length, val, max, min, xpos, bottomypos, gap = 5, buttonSize = 8):
        self.screen = screen
        self.val = val  # start value
        self.max = max  # maximum at slider position right
        self.min = min  # minimum at slider position left
        self.xpos = xpos  # x-location on screen
        self.hit = False  # the hit attribute indicates slider movement due to mouse interaction
        self.length = length
        self.font = font

        # Static graphics - slider background #
        self.txt_surf = font.render(name, 1, BLACK)
        txtHeight = self.txt_surf.get_rect()[3]
        height = 3 * txtHeight + 4 * gap
        self.ypos = bottomypos - height
        self.txt_rect = self.txt_surf.get_rect(center=(self.length / 2, gap + txtHeight / 2))

        self.min_txt_surf = self.font.render(str(min), 1, BLACK)
        self.min_txt_rect = self.min_txt_surf.get_rect(topleft=(gap, 2 * gap + txtHeight))

        self.max_txt_surf = self.font.render(str(max), 1, BLACK)
        self.max_txt_rect = self.max_txt_surf.get_rect(topright=(self.length - gap,  2 * gap + txtHeight))

        self.txt_ypos = height - gap
        self.surf = pygame.surface.Surface((self.length, height))

        self.surf.fill(GREY)

        self.xGap = self.min_txt_rect[2] + gap * 2 + buttonSize / 2
        self.button_ypos = 2 * gap + txtHeight * 1.5
        self.sliderLength = self.length - self.xGap - self.max_txt_rect[2] - gap * 2 - buttonSize / 2
        stripeWidth = 5
        pygame.draw.rect(self.surf, WHITE, [self.xGap, self.button_ypos - 0.5 * stripeWidth, self.sliderLength, stripeWidth], 0)

        # These surfaces never change
        self.surf.blit(self.txt_surf, self.txt_rect)
        self.surf.blit(self.min_txt_surf, self.min_txt_rect)
        self.surf.blit(self.max_txt_surf, self.max_txt_rect)

        # dynamic graphics - button surface #
        self.button_surf = pygame.surface.Surface((20, 20))
        self.button_surf.fill(TRANS)
        self.button_surf.set_colorkey(TRANS)
        pygame.draw.circle(self.button_surf, GREEN, (10, 10), buttonSize, 0)

    def draw(self):
        """ Combination of static and dynamic graphics in a copy of
    the basic slide surface
    """
        # static
        surf = self.surf.copy()

        # dynamic
        pos = (self.xGap + float((self.val - self.min) / (self.max - self.min) * self.sliderLength), self.button_ypos)
        self.button_rect = self.button_surf.get_rect(center=pos)
        surf.blit(self.button_surf, self.button_rect)
        self.button_rect.move_ip(self.xpos, self.ypos)  # move of button box to correct screen position
        self.val_txt_surf = self.font.render(str(round(self.val, 2)), 1, BLACK)
        self.val_txt_rect = self.val_txt_surf.get_rect(midbottom=(self.length / 2, self.txt_ypos))
        surf.blit(self.val_txt_surf, self.val_txt_rect)

        # screen
        self.screen.blit(surf, (self.xpos, self.ypos))

    def move(self):
        """
    The dynamic part; reacts to movement of the slider button.
    """
        self.val = (pygame.mouse.get_pos()[0] - self.xpos - self.xGap) / self.sliderLength * (self.max - self.min) + self.min
        if self.val < self.min:
            self.val = self.min
        if self.val > self.max:
            self.val = self.max