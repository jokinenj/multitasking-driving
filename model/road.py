from math import tan, atan

import pygame
import pygame.gfxdraw

from guiWidgets.pySlider import Slider
from guiWidgets.pyCheckbox import Checkbox

pygame.init()


#STATIC VARIABLES
#Clock
#windowclock = pygame.time.Clock()

#Font
font = pygame.font.SysFont("monospace", 15)

#Colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
GRAY =  (175, 175, 175)
LIGHTGRAY = (200, 200, 200)
BLUE =  (  0,   0, 255)
GREEN = ( 30, 175,  40)
RED =   (255,   0,   0)
YELLOW =   (255,   255,   0)


#Window Information
width = 1200
gui_height = 250
height = 600
size = [width, height + gui_height]
screen = pygame.display.set_mode(size)

#GUI
btn_gap = 10
btn_len = 90
txt_height = 15
btn_height = txt_height * 2 + 3 * btn_gap
learn_btn_rect = (btn_gap, height + gui_height - btn_height - btn_gap - 50, btn_len, btn_height)
learn_txt_rect = (learn_btn_rect[0] + 10, learn_btn_rect[1] + btn_gap, btn_len - btn_gap, txt_height)
learn_txt_2_rect = (learn_btn_rect[0] + 5, learn_txt_rect[1] + btn_gap + txt_height, btn_len - btn_gap, txt_height)
learn_btn = pygame.Rect(learn_btn_rect)


observe_btn_rect = (btn_gap * 2 + btn_len, learn_btn_rect[1], btn_len, btn_height)
observe_txt_rect = (observe_btn_rect[0] + 10, observe_btn_rect[1] + btn_gap, btn_len - btn_gap, txt_height)
observe_txt_2_rect = (observe_btn_rect[0] + 5, observe_txt_rect[1] + btn_gap + txt_height, btn_len - btn_gap, txt_height)
observe_btn = pygame.Rect(observe_btn_rect)

update_slider_val = False

checkbox_size = 12


#Road dimensions
lower_width = 0.3 * width
upper_width = 0.5 * lower_width
half_lower_width = 0.5 * lower_width
half_upper_width = 0.5 * upper_width
default_angle = atan(0.25 * lower_width / height) #= 0.11202896242598788

middle_point = 0.5 * height
LLpt = (0.5 * width - half_lower_width, height)
LRpt = (lower_width + LLpt[0], height)

maxmidy = 0.75 * height
minmidy = 0.25 * height



#Image sources:
#Grass  https://www.safaribooksonline.com/library/view/learning-unity-2d/9781783559046/graphics/9046OT_06_02.jpg
#Car    https://openclipart.org/image/2400px/svg_to_png/190173/SimplePurpleCarTopView.png
#Wheel  http://www.clker.com/cliparts/u/H/N/G/p/B/steering-wheel-hi.png

background = pygame.image.load('imgs/grass.jpg').convert()

#Car variables
car_road_divider = 3
car_dist_from_bottom = 5
car = pygame.image.load('imgs/car.png').convert_alpha()
car_width = int(lower_width / car_road_divider)
car_height = int(car.get_rect()[3] * car_width / car.get_rect()[2])
car = pygame.transform.scale(car, (car_width, car_height))
default_car_X = LLpt[0] + half_lower_width - car_width / 2
default_car_Y = height - car_height - car_dist_from_bottom

#Steering wheel
wheel = pygame.image.load('imgs/steering_wheel.png').convert_alpha()
wheel_position = [width / 3, height + 0, wheel.get_rect()[2] , wheel.get_rect()[3]]
#Stripe dimensions
stripe_width = 10
stripe_length = 50
stripe_gap = 50
stripe_amount = int(height / (stripe_length + stripe_gap)) + 1
update_length = 5

#Main Class
class Road(object):
    def __init__(self, road_width):
        self.done = False
        self.increase = True
        self.lower_height = height / 2
        self.upper_height = height - self.lower_height
        self.upper_angle = default_angle
        self.bottom_angle = default_angle
        self.update_road_corners()
        self.init_stripes()
        self.goal_angle_reached = True
        self.goal_angles = (default_angle, default_angle)
        self.background_pos = 0
        self.driving_data = {}
        self.buttons_enabled = False
        self.learn_enabled = True
        self.observe_enabled = True
        self.learn_btn_txt = "DISABLE"
        self.observe_btn_txt = "DISABLE"
        self.model = 0
        # Road borders as car position returned by the model
        self.minx = -road_width
        self.maxx = road_width

        self.slider = Slider(screen, font, "Car Position", length =observe_btn_rect[0] + btn_len - learn_btn_rect[0],
                             val = 0, max = 1.2 * road_width, min = -1.2 * road_width,
                             xpos = learn_btn_rect[0], bottomypos =learn_btn_rect [1] - btn_gap)
        self.checkbox = Checkbox(screen, font, x = learn_btn_rect[0],
                                 y =self.slider.ypos - btn_gap - checkbox_size,
                                 caption="Map with car pos")

        pygame.event.clear()

    def event_detection(self):
        for event in pygame.event.get():  # User did something
            if event.type == pygame.QUIT:  # If user clicked close
                self.done = True  # Flag that we are done so we exit this loop
                return
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: #1 is the left mouse button
                    if self.buttons_enabled:
                        if learn_btn.collidepoint(event.pos):
                            self.switch_learn_btn_state()

                        elif observe_btn.collidepoint(event.pos):
                            self.switch_observe_btn_state()

                    elif self.slider.button_rect.collidepoint(pygame.mouse.get_pos()):
                        self.slider.hit = True
            elif event.type == pygame.MOUSEBUTTONUP:
                self.slider.hit = False
            self.checkbox.update_checkbox(event)

        self.done = False

    def switch_learn_btn_state(self):
        if self.learn_enabled:
            self.learn_btn_txt = "ENABLE"
            self.learn_enabled = False
        else:
            self.learn_btn_txt = "DISABLE"
            self.learn_enabled = True


    def switch_observe_btn_state(self):
        if self.observe_enabled:
            self.observe_btn_txt = "ENABLE"
            self.observe_enabled = False
        else:
            self.observe_btn_txt = "DISABLE"
            self.observe_enabled = True

    def update_road_corners(self):
        ul = LLpt[0] + height * tan(self.upper_angle)
        self.ULpt = [ul, 0]
        ur = ul + upper_width
        self.URpt = [ur, 0]

        right_angle = atan((LRpt[0] - self.URpt[0]) / height)

        #if abs(defaultAngle - self.angle) < 0.15:
        #    deviation = 1 + (self.angle - defaultAngle)
        #else:
        #    deviation  = 1.15

        if self.upper_angle >= default_angle:
            y = middle_point * self.bottom_angle # self.GetMidPointYPos(middlePoint * self.bottomAngle)

            self.MLpt = (self.ULpt[0] - middle_point * tan(self.bottom_angle) * self.bottom_angle,
                         y)
            self.MRpt = self.get_midpoint_right_X_pos()
        else:
            y = middle_point * right_angle #self.GetMidPointYPos(middlePoint * rightAngle)
            self.MLpt = (self.ULpt[0] - middle_point * tan(right_angle) * self.bottom_angle,
                         y)
            self.MRpt = self.get_midpoint_right_X_pos()

        #pygame.draw.aaline(screen, BLACK, self.MLpt, self.MRpt, 1)

    def get_midpoint_right_X_pos(self):
        return (self.MLpt[0] + 2 * (tan(default_angle) * self.MLpt[1]) + upper_width, self.MLpt[1])

    def get_midpoint_Y_pos(self, val):
        y = val
        #if y > height:
        #    y = height
        #elif y < 0.25 * height:
        #    y = 0.25 * height
        return y
    # def UpdateRoadCorners(self):
    #     ul = LLpt[0] + height * tan(self.angle)
    #     self.ULpt = [ul, 0]
    #     ur = ul + upperWidth
    #     self.URpt = [ur, 0]
    #     rightAngle = atan((LRpt[0] - self.URpt[0]) / height)
    #
    #     if abs(defaultAngle - self.angle) < 0.15:
    #         deviation = 1 + (self.angle - defaultAngle)
    #     else:
    #         deviation  = 1.15
    #
    #     if self.angle >= defaultAngle:
    #         self.MLpt = (self.ULpt[0] - middlePoint * tan(self.angle * deviation) * self.angle,
    #                      middlePoint * self.angle)
    #         self.MRpt = (self.URpt[0] + middlePoint * tan(self.angle * deviation) * rightAngle,
    #                      middlePoint * self.angle)
    #     else:
    #         self.MLpt = (self.ULpt[0] - middlePoint * tan(rightAngle * deviation) * self.angle,
    #                      middlePoint * rightAngle)
    #         self.MRpt = (self.URpt[0] + middlePoint * tan(rightAngle * deviation) * rightAngle,
    #                      middlePoint * rightAngle)

    def draw_road_polygon(self):
        pygame.gfxdraw.filled_polygon(screen, [LLpt, LRpt, self.MRpt, self.URpt, self.ULpt, self.MLpt], GRAY)
        #pygame.draw.aaline(screen, BLACK, self.LRpt, self.URpt, 1)

        #pygame.gfxdraw.bezier(screen, [self.LLpt, (self.ULpt[0] - 0.25*height * tan(self.angle)*0.001*sign, 0.25*height),  self.ULpt], 2, BLUE)

        #pygame.draw.line(screen, RED, [self.ULpt[0] + self.halfUpperWidth, 0], [self.ULpt[0] + self.halfUpperWidth + (self.MLpt[1]*tan(upperAngle)), self.MLpt[1]], 10)
        #pygame.draw.line(screen, RED, [self.MLpt[0] + halfMiddleWidth, self.MLpt[1]], [self.MLpt[0] + halfMiddleWidth + (lowHeight*tan(lowerAngle)), height], 10)

    def calc_stripe_coords(self, length, top_y):
        bottom_y = top_y + length

        upper_angle, lower_angle, half_middle_width = self.get_stripe_angle()

        if top_y <= self.MLpt[1]:
            stripe_angle = upper_angle
            x = self.ULpt[0] + half_upper_width
            relative_top_y = top_y

        else:
            stripe_angle = lower_angle
            x = self.MLpt[0] + half_middle_width
            relative_top_y = top_y - self.MLpt[1]

        top_x = x + relative_top_y * tan(stripe_angle)
        bottom_x = x + (relative_top_y + length) * tan(stripe_angle)

        return top_x, bottom_x, bottom_y

    def init_stripes(self):
        top_y = 0
        top_x, bottom_x, bottom_y = self.calc_stripe_coords(stripe_length, top_y)
        self.stripe_tops = [(top_x, top_y)]
        self.stripe_bottoms = [(bottom_x, bottom_y)]

        for i in range(stripe_amount - 1):
            top_y = self.stripe_bottoms[i][1] + stripe_gap
            top_x, bottom_x, bottom_y = self.calc_stripe_coords(stripe_length, top_y)
            self.stripe_tops.append((top_x, top_y))
            self.stripe_bottoms.append((bottom_x, bottom_y))


    def update_stripes(self):
        for i in range (stripe_amount):
            top_y = self.stripe_tops[i][1] + update_length + self.driving_data['Speed']
            top_x, bottom_x, bottom_y = self.calc_stripe_coords(stripe_length, top_y)
            self.stripe_tops[i] = (top_x, top_y)
            self.stripe_bottoms[i] = (bottom_x, bottom_y)

        #Check if any stripe is out of border
        for i in range(stripe_amount):
            if self.stripe_tops[i][1] > height:
                top_y = 0
                top_x, bottom_x, bottom_y  = self.calc_stripe_coords(stripe_length, top_y)
                self.stripe_tops[i] = (top_x, top_y)
                self.stripe_bottoms[i] = (bottom_x, bottom_y)

            if self.stripe_bottoms[i][1] > height:
                top_y = 0
                top_x, bottom_x, bottom_y = self.calc_stripe_coords(self.stripe_bottoms[i][1] - height, top_y)
                pygame.draw.line(screen, WHITE, [top_x, top_y], [bottom_x, bottom_y], stripe_width)


    def draw_white_stripes(self):
        for i in range(stripe_amount):
            if self.stripe_tops[i][1] < self.MLpt[1] and self.stripe_bottoms[i][1] > self.MLpt[1]:
                upper_angle, lower_angle, half_middle_width = self.get_stripe_angle()

                mid_x = self.ULpt[0] + half_upper_width + self.MLpt[1] * tan(upper_angle)

                bot_x = self.MLpt[0] + half_middle_width + (self.stripe_bottoms[i][1] - self.MLpt[1]) * tan(lower_angle)

                pygame.draw.lines(screen, WHITE, False, (self.stripe_tops[i], (mid_x, self.MLpt[1]),
                                                         (bot_x, self.stripe_bottoms[i][1])), stripe_width)
            else:
                pygame.draw.line(screen, WHITE, self.stripe_tops[i], self.stripe_bottoms[i], stripe_width)


    def get_stripe_angle(self):
        half_middle_width = (self.MRpt[0] - self.MLpt[0]) / 2
        up_height = self.MLpt[1]
        low_height = height - up_height
        up_X_dist = self.MLpt[0] + half_middle_width - (self.ULpt[0] + half_upper_width)
        low_X_dist = LLpt[0] + half_lower_width - (self.MLpt[0] + half_middle_width)
        upper_angle = atan(up_X_dist / up_height)
        lower_angle = atan(low_X_dist / low_height)

        return upper_angle, lower_angle, half_middle_width


    def scroll_background(self):
        rel_backgroundPos = self.background_pos % background.get_rect().width
        screen.blit(background, (0, rel_backgroundPos - background.get_rect().width))
        if rel_backgroundPos > height:
            screen.blit(background, (0, rel_backgroundPos))
        self.background_pos += update_length * self.driving_data['Speed']

    def get_scaled_value(self, x, out_min, out_max):
        return out_min + (out_max - out_min) * (x - self.minx) / (self.maxx - self.minx)

    def draw_car_and_wheel(self):
        #Update Car
        scaled_pos = self.get_scaled_value(self.driving_data['Position'], LLpt[0], LRpt[0]) - car_width / 2
        rotate_car, rotate_rect = self.rot_center(car, self.driving_data['Direction'], False)
        screen.blit(rotate_car, dest = (scaled_pos, default_car_Y), area = rotate_rect)

        #Update Wheel
        rotate_wheel, rotate_rect = self.rot_center(wheel, self.driving_data['Action'], True, wheel_position)
        screen.blit(rotate_wheel, rotate_rect)

        space = 20
        for var in self.driving_data:
            label = font.render(var + ": " + "{0:.2f}".format(self.driving_data[var]),
                                1, BLUE)
            screen.blit(label, (wheel_position[0] + wheel_position[2] + 20,
                                wheel_position[1] + space))
            space += 20

    # https://www.pygame.org/wiki/RotateCenter
    def rot_center(self, image, angle, center = True, offset = None):
        rect = image.get_rect()

        if offset != None:
            for i in range(len(rect)):
                rect[i] = offset[i]

        """rotate an image while keeping its center"""
        rot_image = pygame.transform.rotozoom(image, - angle, 1)
        rot_rect = rot_image.get_rect()#center=rect.center
        if center:
            setattr(rot_rect, 'center', rect.center)

        return rot_image, rot_rect

    def update_variables(self, action, carpos, carspeed, cetime):
        # Convert action from rad to deg, and smooth by taking mean of lag and current.        
        self.driving_data['Action'] = (action * 57.29578)# + self.driving_data.get('Action', 0)) / 2
        self.driving_data['Position'] = carpos
        self.driving_data['Direction'] = (action * 57.29578)# + self.driving_data.get('Direction', 0)) / 2
        self.driving_data['Speed'] = carspeed
        self.driving_data['Time'] = cetime
        

    def update_driving_data(self, varDict):
        self.driving_data = varDict

        #TODO: add upper and lower angle update


    def set_road_curvature(self, top_angle = default_angle, bottom_angle = default_angle, step = 0.01):
        if top_angle != default_angle or bottom_angle != default_angle:
            if self.goal_angle_reached:
                self.goal_angles = (top_angle, bottom_angle)

                self.goal_angle_reached = False

            else:
                if round(self.upper_angle, 2) > round(self.goal_angles[0], 2):
                    self.upper_angle -= step
                elif round(self.upper_angle, 2) < round(self.goal_angles[0], 2):
                    self.upper_angle += step

                if round(self.bottom_angle, 2) > round(self.goal_angles[1], 2):
                    self.bottom_angle -= step
                elif round(self.bottom_angle, 2) < round(self.goal_angles[1], 2):
                    self.bottom_angle += step
                #print(self.MLpt[1], (round(self.goalAngles[0], 2), round(self.goalAngles[1], 2)), (round(self.upperAngle, 2), round(self.bottomAngle, 2)))
                if round(self.upper_angle, 2) == round(self.goal_angles[0], 2) and\
                                round(self.bottom_angle, 2) == round(self.goal_angles[1], 2):
                    self.goal_angle_reached = True


    def draw_gaze(self):
        pygame.draw.circle(screen, RED, (int(width * 0.5), int(height * 0.25)), 10)

    def draw_gaze_nowhere(self):
        pygame.draw.circle(screen, RED, (int(width * 0.75), int(height * 0.65)), 10)

    def draw_buttons(self):
        pygame.draw.rect(screen, LIGHTGRAY, learn_btn)
        screen.blit(font.render(self.learn_btn_txt, True, BLACK), learn_txt_rect)
        screen.blit(font.render("LEARNING", True, BLACK), learn_txt_2_rect)

        pygame.draw.rect(screen, LIGHTGRAY, observe_btn)
        screen.blit(font.render(self.observe_btn_txt, True, BLACK), observe_txt_rect)
        screen.blit(font.render("OBSERVING", True, BLACK), observe_txt_2_rect)

    def draw_road(self):
        #clock = pygame.time.Clock()
        if not self.done:
            #clock.tick(10)
            self.scroll_background()

            self.draw_road_polygon()
            self.update_road_corners()
            #self.draw_white_stripes()
            #self.update_stripes()
            pygame.draw.rect(screen, WHITE, (0, height, width, gui_height))

            self.draw_car_and_wheel()

            screen.blit(font.render("Model: " + str(self.model),1, BLUE),
                        (wheel_position[0] - 100, wheel_position[1] + 20))

            if self.buttons_enabled:
                self.draw_buttons()
            self.event_detection()
            if self.slider.hit:
                self.slider.move()
            else:
                if self.checkbox.checked:
                    self.slider.val = self.driving_data['Position']
            self.slider.draw()
            self.checkbox.render_checkbox()
            pygame.display.update()

        else:
            pygame.quit()
