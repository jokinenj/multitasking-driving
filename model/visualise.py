# Visualise the multitasking model.

import numpy as np
import time
import search
import driver
import multitasking

import road


rows = 3
cols = 4
speed = 33
s = search.search(rows, cols)
s.q = np.load("models/search-" + str(rows) + "_" + str(cols) + ".npy").item()
d = driver.driver(speed)
d.transitions = np.load("models/driver-t-" + str(speed) + ".npy").item()
d.q = np.load("models/driver-" + str(speed) + ".npy").item()
m = multitasking.multitasking(d, s)
m.q = np.load("models/multi" + str(speed) + "-" + str(rows) + "_" + str(cols) + ".npy").item()
d.learning = False
s.learning = False
m.learning = False
d.softmax_temp = 0.1
s.softmax_temp = 0.1
m.softmax_temp = 0.1

r = road.Road(1)
import device
dev = device.Device(road.screen, rows, cols, road.width, road.height+10)

until = 100
while m.model_time < until:
    time_update = m.model_time
    m.do_iteration()
    print(m.belief, m.q[m.belief[0]][m.belief[1]][m.belief[2]])
    r.update_variables(action = m.d.action,
                          carpos = m.d.pos,     
                          carspeed = m.d.speed,
                          cetime = m.model_time)
    r.draw_road()
    if m.action == "drive": r.draw_gaze()
    if m.action == "search": dev.is_searching = True
    else: dev.is_searching = False
    dev.update_sm_data(m.s.eye_loc, m.s.action, m.s.target, np.copy(m.s.obs).flatten())
    dev.DrawDevice()
    time.sleep(m.model_time - time_update)
