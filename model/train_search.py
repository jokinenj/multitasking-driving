# Train search model for use in multitasking.

import search
import numpy as np


s = search.search(3, 4)
s.log_p = True
s.learn_model(1600000, debug = True)
np.save("models/search-3_4.npy", s.q)
s.write_data_to_file("output/search-3_4-training.csv")

s = search.search(3, 2)
s.log_p = True
s.learn_model(1600000, debug = True)
np.save("models/search-3_2.npy", s.q)
s.write_data_to_file("output/search-3_2-training.csv")

