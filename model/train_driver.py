# Train driver for use in multitasking.

import driver
import numpy as np

d = driver.driver(33)
d.log_p = True
d.learn_transitions(iters = 1000, debug = True)
d.learn_model(350000, close_eyes = 0.1, close_duration = 3, debug = True)
d.write_data_to_file("output/driver-33-training.csv")
np.save("models/driver-t-33.npy", d.transitions)
np.save("models/driver-33.npy", d.q)

d = driver.driver(17)
d.log_p = True
d.learn_transitions(iters = 1000, debug = True)
d.learn_model(350000, close_eyes = 0.1, close_duration = 3, debug = True)
d.write_data_to_file("output/driver-17-training.csv")
np.save("models/driver-t-17.npy", d.transitions)
np.save("models/driver-17.npy", d.q)
