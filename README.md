# Multitasking in Driving

Code and data for Jokinen, Kujala, & Oulasvirta (2021). Multitasking in Driving As Optimal Adaptation under Uncertainty. Human Factors.
https://acris.aalto.fi/ws/portalfiles/portal/76868713/ELEC_Jokinen_etal_Multitasking_in_Driving_as_Optimal_Adaptation_Human_Factors_2021.pdf
